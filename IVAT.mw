__TOC__
[[Image:IVAT_Platine_01.jpg|thumb|right|IAVT-Platine Version 1.2]]

Der IVAT ('''I'''nter'''va'''ll-'''T'''rigger) ist ein interaktiv einstellbarer und universeller Auslöser für Fotokameras. Mit dieser Fernsteuerung kann man seine Kamera in vielen Bereichen bequem sich selbst überlassen, ohne, z.B. für Aufnahmeserien, immer "vor Ort" sein zu müssen. 
Das Layout basiert auf dem Arduino-Board, allerdings wurden alle für den Betrieb als Auslöser nicht notwendigen Teile entfernt um Kosten und Platz zu sparen.

== Funktionen ==
*Auslöser
**Fokussierung
**Auslösen...
*Intervallfunktion
**Einstellen des Intervalls (Abstand zwischen zwei Bildern)
**Einstellen der Anzahl aufzunehmender Bilder
**Anzeigen der verbleibenden Dauer und Bilder
*Stromversorgung
**Netzteil und Batteriebetrieb
**schön wäre es, die Stromversorgung der Kamera mit nutzen zu können

===Zusatzfunktionen===
Diese sind zweitrangig und werden dann eventuell in späteren Versionen nachgereicht...
*Funkauslöser, alternativ IR-Auslöser (wird von mir präferiert da sicher günstiger zu realisieren)
*fotoempfindliche Auslösung (Lichtschranke)
*soundempfindliche Auslösung 
**wohl eher sinnlos, außer für spezielle Auslösesituationen, wie z.Bsp. wenn etwas im Moment des Aufschlags (Ei auf Boden, Tennisball auf Schläger, ...) fotografiert werden soll

==Software==
[[Datei:IVAT_1_2.png|miniatur|Menüführung]]
Das Ganze wird über ein Mini-Menü gesteuert, dass auf einem 16x2 LCD Display dargestellt wird. Rechts im Zustandsdiagramm sieht man die einzelnen Zustände über welche die Menüführung realisiert wird.

Es existieren folgende Menüpunkte:
*Intervall
**Ja/Nein
**Falls Ja, dann weitere Menüpunkte:
***Amount -> Anzahl Bilder einstellen
***bis zu 2^32 Bildern ~ 4.000.000.0 Bilder, allerdings können max. die Einstellungen für 9.000.000 Bilder ordentlich auf dem kleinen LCD angezeigt werden, dass sollte aber trotzdem die Kapazität der meisten Speicherkarten überfordern...
***Intervallzeit = Abstand zwischen den einzelnen Bildern
***bis zu 2^32 Sekunden ~ 50.000 Tage, hier gilt das gleiche wie bei der Anzahl der Bilder, irgendwann ist Schluss in der Anzeige, aber so große Werte machen auch nicht wirklich Sinn

*Delay 
**Auswahl Verzögerung bis (zum ersten) Auslösen
**bis zu 2^32 Sekunden

*Fokus
**Auswahl automatische Fokussierung Ja/Nein
**Falls Ja, dann auswahl:
***vor jedem Bild
***nur vor erstem Bild

{| class="wikitable"
|+Fokusstatus mit deaktivierter Intervalfunktion
! Anzeige !! Bedeutung !! Int. Status
|-
| n || no || 0
|-
| y || yes || 1
|}
{| class="wikitable"
|+Fokusstatus mit aktivierter Intervalfunktion
! Anzeige !! Bedeutung !! Int. Status !! Bemerkung
|-
| n || no || 0 || nie fokussieren
|-
| yo || yes, once || 2 || vor jedem Bild erneut fokussieren
|-
| ya || yes, always || 3 || vor jedem Bild erneut fokussieren
|}

==Hardware== 
===IVAT 1.2 Hardware===
[[Datei:IVAT_HW_1.2.jpg|miniatur|Layout für Lochrasterplatinen]]
Die erste Version, welche vollständig ohne das Arduino-Board auskommt ist vollendet. Es hat recht lange gedauert, es ist viel dazwischen gekommen und ich habe drei Anläufe gebraucht bis alles so lief wie ich mir das gewünscht habe, aber letztendlich ist alles gut gegangen.

Ich habe mich vorerst gegen das Ätzen einer eigenen Platine entschieden, da ich hier nicht die Mittel dazu habe. Zwar möchte ich das ganze möglichst klein bauen, dem Ganzen sind aber alleine durch die Größe des Displays und der vier Cursortasten physikalische Grenzen gesetzt, sodass ohnehin nicht die Platinengröße der entscheidende Faktor bei der Miniaturisierung ist.

Es ist also ein Design für Streifenrasterplatinen entstanden, bei dem aber nichtsdestotrotz versucht wurde, die höchste Packungsdichte zu erreichen. Das mit dem wundervollen Programm "Lochmaster" designte Layout ist rechts dargestellt. Eine PDF-Version gibt es [http://www.gasthof-klosterbuch.de/zeug/IVAT/IVAT_1.2_all_A4.pdf hier].

====Bauteil-Liste====
Hier eine Liste aller benötigten Bauteile. Das ganze gibt es auch direkt als Warenkorb für [https://secure.reichelt.de/?;ACTION=20;LA=5010;AWKID=7875;PROVID=2084 Reichelt]. Und nicht das [http://shop.ebay.de/i.html?_nkw=LCD+HD44780+2x16&_sacat=0&_sop=15&_odkw=LCD+HD44780+16&_osacat=0&_trksid=p3286.c0.m270.l1313 LCD-Display] von Ebay vergessen!
{| class="wikitable"
! Bauteil !! Beschreibung !! Anzahl !! bei Conrad !! bei Reichelt!
|-
| Streifenrasterplatine || Hartpapier || 1 || [http://www.conrad.de/ce/de/product/527629/Hartpapier-Platine-12-50-X-90/2512030] || [http://www.reichelt.de/?;ACTION=3;LA=444;GROUP=C932;GROUPID=3373;ARTICLE=8279;START=0;SORT=artnr;OFFSET=16;SID=32TQjC038AAAIAADs769M95a14c21bd67ae3686eee8413ba2baae]
|-
| Widerstand || 10k-Ohm || 5 || [http://www.conrad.de/ce/de/product/405370/WIDERSTAND-KOHLE-05-W-5-10K-BF-0411/SHOP_AREA_17443&promotionareaSearchDetail=005] || [http://www.reichelt.de/?;ACTION=3;LA=5;GROUP=B1115;GROUPID=3066;ARTICLE=1338;START=0;SORT=user;OFFSET=16;SID=32TQjC038AAAIAADs769M95a14c21bd67ae3686eee8413ba2baae]
|-
| Kondensator || 22pF || 2 || [http://www.conrad.de/ce/de/product/457167/KERAMISCHER-KONDENSATOR-22-PF/SHOP_AREA_17436&promotionareaSearchDetail=005] || [http://www.reichelt.de/?;ACTION=3;LA=444;GROUP=B353;GROUPID=3169;ARTICLE=9330;START=0;SORT=artnr;OFFSET=16;SID=32TQjC038AAAIAADs769M95a14c21bd67ae3686eee8413ba2baae]
|- 
| Kondensator || 100nF || 1 || [http://www.conrad.de/ce/de/product/500956/KERAMIK-KONDENSATOR-VS-PRINT-X7R-100-NF/SHOP_AREA_17436&promotionareaSearchDetail=005] || [http://www.reichelt.de/?;ACTION=3;LA=444;GROUP=B353;GROUPID=3169;ARTICLE=9265;START=0;SORT=artnr;OFFSET=16;SID=32TQjC038AAAIAADs769M95a14c21bd67ae3686eee8413ba2baae]
|-
| Miniatur-Trimmer || 10k-Ohm || 1 || [http://www.conrad.de/ce/de/product/422576/MIN-POTENTIOMETER-CA6-H-10K-LIN-STEHEND] || [http://www.reichelt.de/?;ACTION=3;LA=5;GROUP=B24;GROUPID=3131;ARTICLE=3500;START=0;SORT=user;OFFSET=16;SID=32TQjC038AAAIAADs769M95a14c21bd67ae3686eee8413ba2baae]
|-
| BC 549B || NPN Transistor, Gehäuse TO-92,  [http://www.produktinfo.conrad.com/datenblaetter/150000-174999/154725-da-01-en-BC549B.pdf Datenblatt] || 2 || [http://www.conrad.de/ce/de/product/154725/TRANSISTOR-BC-549-B/SHOP_AREA_37318&promotionareaSearchDetail=005] || [http://www.reichelt.de/?;ACTION=3;LA=444;GROUP=A121;GROUPID=2881;ARTICLE=5011;START=0;SORT=artnr;OFFSET=16;SID=32TQjC038AAAIAADs769M95a14c21bd67ae3686eee8413ba2baae]
|-
| IC-Fassung || 28-polig, 300mil = 7,62mm || 1 || [http://www.conrad.de/ce/de/product/189515/IC-FASSUNG-28POLIG/SHOP_AREA_27787&promotionareaSearchDetail=005] || [http://www.reichelt.de/?ACTION=3;GROUP=C131;GROUPID=3215;ARTICLE=86281;SID=32TQjC038AAAIAADs769M95a14c21bd67ae3686eee8413ba2baae]
|-
| 16 MHz Quarz || HC-49/U || 1 || [http://www.conrad.de/ce/de/product/168750/QUARZ-16000000-MHZ-HC-49U/SHOP_AREA_29142&promotionareaSearchDetail=005] || [http://www.reichelt.de/?;ACTION=3;LA=5000;GROUP=B41;GROUPID=3173;ARTICLE=32852;START=0;SORT=artnr;OFFSET=16;SID=32TQjC038AAAIAADs769M95a14c21bd67ae3686eee8413ba2baae]
|-
| Taster || Bauform je nach Wunsch || 4 || [http://www.conrad.de/ce/de/product/701743/TASTSCHALTER-FSM100/SHOP_AREA_17384] || [http://www.reichelt.de/?;ACTION=3;LA=444;GROUP=C223;GROUPID=3278;ARTICLE=27892;START=0;SORT=artnr;OFFSET=100;SID=32TQjC038AAAIAADs769M95a14c21bd67ae3686eee8413ba2baae]
|-
| LCD-Display || 2 Zeilen, 16 Zeichen (=2x16) || 1 || [http://shop.ebay.de/i.html?_nkw=LCD+HD44780+2x16&_sacat=0&_sop=15&_odkw=LCD+HD44780+16&_osacat=0&_trksid=p3286.c0.m270.l1313 Besser bei Ebay]
|}
Der Gesamtpreis bei Reichelt beträgt 12,06€, bei Conrad sind es 10,08€. Bei Reichelt sind die Kosten für die eigentlichen Bauteile allerdings lediglich 3,46€(!!!), bei Conrad sind es 5,13€. Der Rest sind Gebühren und Versandkosten, es lohnt also eine Sammelbestellung abzuwarten.

Wer nicht seinen guten ATmega328-Chip verbauen möchte, ohne den so ein Arduino-Board ja doch eher nutzlos ist, der hat verschiedene Möglichkeiten. Es ist momentan einigermaßen schwierig, an die ATmega328-Chips heran zu kommen. Bei Conrad und Reichelt konnte ich gar keine finden, bei [http://shop.ebay.de/i.html?_nkw=ATmega328&_sacat=0&_odkw=ATmega32&_osacat=0&_trksid=p3286.c0.m270.l1313 Ebay] sieht es auch eher mau aus.  Zum Glück ist mein Programm nicht so riesig, so dass auch die kleinere Version dieses Chips, der ATmega168 ausreicht. Der hat zwar, wie der Name schon verrät nur 16kB Programmspeicher, das reicht uns aber. Man muss aber immer beachten, dass man sich die Version im  '''28-poligen DIP-Gehäuse''' kauft!

Jetzt hat man folgende (nach meiner Präferenz sortierte) Möglichkeiten:

1. Kauf eines ATmegaChips der bereits einen Bootloader besitzt (Infos zum Bootloader gibt es [http://arduino.cc/en/Hacking/Bootloader?from=Main.Bootloader hier]), die gibt es teilweise bei Ebay, ansonsten noch bei [http://www.watterott.com/de/ATmega328-Arduino-Bootloader watterott.com], bei [http://www.lipoly.de/index.php?main_page=product_info&products_id=103958 lipoly.de] oder bei [http://www.sparkfun.com/products/9217 sparkfun.com] und in der kleinen Version auch [http://www.lipoly.de/index.php?main_page=product_info&products_id=103957 hier]. Zumindest bei watterott.com scheinen noch Einige da zu sein.

2. Kauf eines ATmegaChips ohne Bootloader und manuelles brennen des Bootloaders. Dazu benötigt man allerdings entweder den originalen [http://www.atmel.com/dyn/products/tools_card.asp?tool_id=2726 AVR-ISP], man baut sich den [http://www.ladyada.net/make/usbtinyisp/ USBtinyISP] oder man nutzt den [http://arduino.cc/en/Hacking/ParallelProgrammer Parallel Port Programmer]. Alle Infos dazu gibts auch [http://www.lipoly.de/index.php?main_page=product_info&products_id=103957 hier]. Der große Vorteil dieser Variante ist, dass man auch die ATmega168 nutzten kann, die es bei [http://www.reichelt.de/?;ACTION=3;LA=444;GROUP=A363;GROUPID=2959;ARTICLE=58324;START=0;SORT=artnr;OFFSET=100;SID=32TQjC038AAAIAADs769M95a14c21bd67ae3686eee8413ba2baae Reichelt], [http://shop.ebay.de/i.html?_nkw=ATmega168&_sacat=0&_odkw=ATmega328&_osacat=0&_trksid=p3286.c0.m270.l1313 Ebay], [http://www.lipoly.de/index.php?main_page=product_info&products_id=103654 lipoly.de] und [http://shop.myavr.de/Bauelemente%20und%20Controller/ATmega168PA-PU.htm?sp=article.sp.php&artID=200030 myAVR] gibt. Das ganze ist jedoch auch mit Arbeit verbunden und wir wollen ja lieber einen Auslöser bauen!

3. Da auf dem IVAT ein IC-Sockel verbaut wird, kann man immer hin und her tauschen. Das will man aber eigentlich nicht, da irgendwann unweigerlich die Beinchen vom IC abbrechen und wir unseren Arduino oft für andere Zwecke missbrauchen.

==Schaltplan und Platinenlayout==
Nachdem Marcus so nett war und uns eine kleine aber feine Einführung in Eagle verpasst hat, bin ich jetzt endlich dazu gekommen einen ersten Entwurf in Eagle zu realisieren. Die Platine soll am Ende selbst hergestellt und gelötet werden, daher bin ich noch skeptisch ob die Maße so funktionieren...

[[Datei:IVAT-1.0-circuit_diagram.png|rahmenlos|ohne|miniatur|Schaltplan]]

[[Datei:IVAT-1.0-schematic.png|rahmenlos|ohne|miniatur|Layout]]

==(Vermutlich) Unterstützte Kameramodelle==
Da es wohl einige Kameras mit einem Mikro-Klinke Eingang gibt, deren Funktion sich wohl nicht grundsätzlich voneinander unterscheiden dürfte (sind ja nur 3 Anschlüsse), gibt es einige Kameras an denen der gute IVAT funktionieren dürfte...

Dazu zählen vermutlich: 
* Canon EOS: 1000D, 550D, 500D, 450D, 400D, 350D, 300D, 300Dx, 300v, 300, 3000, 30, 30v, 33, 33v, 50, 50e, 500, 5000
* Pentax: K-m, K200D, K110D, K100D, K20D, K10D, *istD, *istDs, *istDs2, *istDL, MZ-6, ZX-L, MZ-L Modelle
* Contax N645, 645, N1, NX, N Digital
* GX-1L, GX-1S, GX-10
(Außer bei der Canon 400D konnte ich das aber natürlich nicht kontrollieren. Aber zumindest bei den anderen Canon-Modellen bin ich mir recht sicher das es auch funktioniert, bei den restlichen Herstellern kommt es darauf an, wie sie das Auslösen realisiert haben.)

==Aktueller Stand==
'' '''09.01.2011''' Ok, es ist viel Zeit ins Land gegangen seit dem letzten Update, aber ich war in der Zeit nicht völlig nutzlos. Ich hab mich im letzten halben Jahr, immer wenn die Zeit es erlaubt hat, darum gekümmert die Hardware des IVAT eigenständig läuft. Dazu musste ich insgesamt bisher drei Arduino-Clone auf einer Lochrasterplatine zusammengelötet, die letzte Version scheint gut zu funktionieren, aber es gab auch schon wieder ein paar Modifikationen, sodass wohl irgendwann auch Version 4 das Licht der Welt erblicken wird. Aber genug von dem was war, viel wichtiger ist der aktuelle Stand. Es gibt eine vollständig eigenständige Version welche nun auch schon 3 Zeitraffer aufnahmen realisiert hat. Die schönste und längste findet man [http://www.youtube.com/watch?v=wcC1mxeo3nY hier], das sind immerhin drei Tage und der Auslöser hat keinerlei Probleme gemacht. Jetzt verbau ich das ganze noch in ein provisorisches Gehäuse, solange bis mir eine schöne und günstige Lösung einfällt das ganze zu umschließen. Es gibt aber immer noch viel zu tun. Die Software ist noch nicht ganz perfekt (falls jemand Lust und Zeit hat meinen C/C++ Code zu kontrollieren, dann wäre ich sehr dankbar), eine Dokumentation fehlt quasi noch völlig, die Stromversorgung funktioniert bisher nur per Netzteil, ...'' 

'' '''30.05.2010''' Durch die großartige Hilfe von DoDo vergangene Woche und durch mein freies Wochenende, funktioniert die Software und ist bereit für einen Testlauf. Die Menüführung und die verschiedenen Einstellungen funktionieren einwandfrei. In den nächsten Tagen werde ich einen Testlauf starten und das ganze mal 2 Tage im Dauerbetrieb laufen lassen, um zu sehen, ob das Programm auch langzeitstabil läuft.'' 

'' '''Älter...'''Der erste große Schritt für mich ist getan, es funktioniert. Ich kann die Kamera über den Mikro-Klinke Eingang in verbindung mit dem Arduino steuern. Auch Reihenaufnahmen und eine grundlegende Steuerung der Intervallfunktion funktionieren. Der nächste und für mich vermutlich schwierigste Schritt, ist jetzt eine Art Textbasierende Menüführung zu programmieren..."

==Sponsor==
An dieser Stelle möchte ich mich recht herzlich bei [http://www.watterott.net/ '''Watterott Electronic'''] für ihre tolle Unterstützung mit Material bedanken. Der [http://www.watterott.com/ Onlineshop] ist eine wahre Fundgrube für Elektronik-Spielereien und bietet vor allem für mich als Arduino Nutzer ein klasse Angebot an Shields und Zubehör. 
Das [http://www.watterott.net/projects/support Supportprogramm], über das auch ich unterstüzt werde, bietet jedem die Chance, sich um Unterstützung zu bewerben. Die Vorraussetzungen um Hilfe zu bekommen sind nicht unmenschlich, die Details gibts auf der Seite.

[[Kategorie:Projekt]]
