[[Datei:1406FreifunkDresden-Logo.svg‎|rechts]]
[[Freifunk Dresden]] ist das Projekt in Dresden zu [[wikipedia:de:Freifunk]]. Das Freifunk-Netz Dresden ist ein von Freiwilligen betriebenes freies WLAN-Netzwerk. Ziel ist es, Dresden flächendeckend mit [[Freifunk]] zu versorgen.<ref>Seite des [http://www.freifunk-dresden.de Freifunk Dresden]</ref>

== Entstehung ==

[[Freifunk Dresden]] wurde ursprünglich unter dem Namen ''ddmesh'' von [[Stephan Enderlein]] gestartet und existiert bereits seit einigen Jahren.

Seit 2014 sind durchgängig mehr als 50 Knoten in Dresden erreichbar und auch Geschäfte in Dresden nehmen die Vorteile von Freifunk für sich und ihre Kunden wahr.

== Mitmachen ==

Um Teil des Freifunk-Netzwerkes zu werden, braucht man sowohl einen handelsüblichen Router als auch die passende Freifunk-Firmware-Version dazu.

=== Firmware ===

Seit {{#formatdate:2014-12-03}} steht die [http://download.ddmesh.de/firmware/2.1.5/ Firmware 2.1.5] zur Verfügung.

Die Firmware-Dateien sind nach den unterstützten Geräten benannt. Der Name einer Firmware-Datei setzt sich als <code>openwrt-ar71xx-generic-''ROUTERNAME''-squashfs-''ZUSATZ''.bin</code> zusammen.
: So ist beispielsweise die Datei <code>openwrt-ar71xx-generic-''tl-wr841n-v8''-squashfs-''factory''.bin</code> für einen Router
:* des Modells ''TL-WR841N''
:* in der ''V''ersion ''8'' (der Hardware)
:*: siehe Etikett auf der Unterseite des Gerätes.
:* ''Factory'' bedeutet, dass diese Firmware-Datei für die Erstinstallation der Freifunk-Firmware auf einem Gerät verwendet werden muss, auf dem sich bisher noch eine andere Firmware befindet, meist die Original-Firmware des Herstellers.
: Beim Dateinamen <code>openwrt-ar71xx-generic-''tl-wr841n-v8''-squashfs-''sysupgrade''.bin</code> bedeutet der Zusatz ''Sysupgrade'' hingegen, dass diese Datei zum Updaten eines bereits für den Dresdner Freifunk im Einsatz befindlichen Routers ist.

Ausführliche Erklärungen zur Konfiguration der Firmware werden im [[freifunk-dresden:Hauptseite | Wiki]] (wiki.freifunk-dresden.de) gepflegt.

=== Router ===

Wie der [http://download.ddmesh.de/firmware/2.1.5/ar71xx/ Firmware-Liste] zu entnehmen ist, gibt es eine Vielzahl an Freifunk-kompatiblen Routern. Allerdings erfreuen sich bestimmte Modelle in Dresden meist insbesondere wegen der günstigen Anschaffungskosten besonderer Beliebtheit:

{| class="wikitable sortable zebra toptextcells"
|-
! Geräte-Name
! Hersteller
! Preis (&euro;)
! RAM (MB)
! ROM (MB)
! class="unsortable" | Beschreibung
|-
| TL-WR841N
| TP-Link
| style="text-align:right" | 16-20
| style="text-align:right" | 32
| style="text-align:right" | 4
| Im Unterschied zum Modell mit der Endung ''ND'' mit nicht abnehmbaren Antennen ausgestattet.
|-
| TL-WR841ND
| TP-Link
| style="text-align:right" | 20-29
| style="text-align:right" | 32
| style="text-align:right" | 4
| ''D'' steht für ''detachable'', der Router besitzt also abnehmbare Antennen.
|-
| TL-WDR3600
| TP-Link
| style="text-align:right" | 40-
| style="text-align:right" | 128
| style="text-align:right" | 8
| mehr Speicher, USB Anschlüsse, auch 5 GHz, 2 abnehmbare Antennen
|-
| TL-WDR4300
| TP-Link
| style="text-align:right" | 45-
| style="text-align:right" | 128
| style="text-align:right" | 8
| mehr Speicher, USB Anschlüsse, auch 5 GHz, 3 abnehmbare Antennen
|-
| Nanostation&nbsp;M2&nbsp;loco
| Ubiquiti
| style="text-align:right" | 40-
| style="text-align:right" | 32
| style="text-align:right" | 8
| 2,4 GHz Richtfunk, Outdoor, 60° Öffnungswinkel, Power over Ethernet + Injector
|-
|}

== Standorte ==

Das Netz von Freifunk Dresden erstreckt sich nicht nur über Dresden selbst, sondern ist mittlerweile vereinzelt u. a. in
* Coswig,
* Freital,
* Meißen,
* Radeberg und
* Radebeul
zu finden. Detaillierte Standortinformationen sind auf der [http://www.freifunk-dresden.de/topology/google-maps.html Freifunk-Karte] einsehbar.

Neben den festen Standorten gibt es Freifunk auch auf [[Freifunk Dresden/Veranstaltung | Veranstaltung]]en, wie z. B. Weihnachtsmärkten.

== Original-Firmware ==

Die Firmware basiert auf [[w:de:OpenWrt|OpenWrt]] mit BMX (Alternativentwicklung basierend auf batmand) und einer custom Weboberfläche.<br />
Das Routingprotokoll batman-adv wird hier nicht verwendet und wurde parallel zu BMX mit einem komplett anderen Ziel entwickelt.

Nutzung der Original-Firmware hat folgende Vorteile:
* Konsistente Konfiguration mit fast allen anderen Teilnehmern
* Anschluß ans Backbone-VPN (vtun)
* Offenes WLAN, welches über das Backbone ins Internet kommt (alternativ durch zusätzliche Pakete ein eigener VPN-Dienst konfigurierbar)
* Webserver der die Knotenmetadaten ausliefert (automatische Knotenregistrierung)

=== Freie Software und Peering ===

Die Software ist frei und kann durch die Quellen von OpenWRT und die Sourcen zu Änderungen von der Freifunk-Seite eingesehen werden. Konfiguration und Scripte sind in den Images zu finden oder auf [https://github.com/ddmesh/firmware-freifunk-dresden GitHub] (Licensen beachten).<br />
Die Dokumentation zum Bau eigner Firmware Images und Alternativ-Knoten sind im Aufbau.

Da zu [[w:de:Freier Software|Freie Software]] für viele von uns auch der freie und möglichst barrierefreiem Zugang zu weiteren Informationen zählt, wollen wir diesen Umstand ändern - auch um Freifunk noch besser machen zu können. Im Widerspruch dazu steht das berechtigte Interesse der bisherigen Freifunk-Teilnehmer auf einen Störungsfreien Betrieb und infolgedessen Imageschaden bei den normalen Benutzern, der durch Experimente mit alternativer Firmware gefährdet sein könnte - das Marketing für Freie Software sowie für Freifunk selbst sind leider schwierig.

Ideen dazu werden [[Diskussion:Freifunk Dresden|diskutiert]], um die jetzigen Betreibenden in Blick auf offenem Zugang zu Informationen zu überzeugen.

== Technische Informationen ==

Grundsätzlich soll das [http://wiki.freifunk-dresden.de/ Wiki Freifunk Dresden] Informationen, insbesondere auch technische Informationen, bereitstellen. Das ist wohl aber noch nicht der Fall und soll wohl in Arbeit sein.

=== Quellcode ===

Der Quellcode soll auf [[#GitHub]] verfügbar sein. Dies gilt insbesondere für die [[#Firmware]].<ref>https://github.com/ddmesh/firmware-freifunk-dresden</ref>

=== mögliche Probleme beim Betrieb der Technik ===

== Metadaten ==

=== Freifunk-API ===

Es existiert eine Inter-Community-API analog zur SpaceAPI. Beispiel: [http://cholin.spline.de/freifunk/api-viewer/ Freifunk API Viewer]

[https://github.com/freifunk/directory.api.freifunk.net/blob/master/directory.json Directory.json], [http://info.ddmesh.de/info/freifunk.json Dresden]

=== Knotenmetadaten ===

Jeder Knoten bietet ein JSON-Dokument unter http://10.200.../sysinfo-json.cgi an. Es enthält Versionsstände, Geokoordinaten, Kontaktinfo, Auslastung und alle Routen.

== IPv4-Meshing mit bmxd ==

=== Bauen ===

Code, letzte Commits im Feb 2011, gibts [https://github.com/axn/bmxd auf Github].

<pre>
git clone git://github.com/axn/bmxd.git
cd bmxd
make -j5
strip bmxd  # Spart Platz
</pre>

=== Voraussetzungen ===

* Lies die [http://man7.org/linux/man-pages/man8/ip-rule.8.html ip-rule Manpage] um die Abarbeitung von Regeln mit mehreren Routing-Tabellen zu verstehen.
* Vergewisser dich ob der Broadcast-Adresse deines Adhoc-Interfaces. Mit ''ip addr add'' stimmte manchmal was nicht.

=== Starten ===

<pre>bmxd dev=adhoc0</pre>

Der Routing-Daemon daemonisiert sofort und schreibt ins Syslog. Mit <code>bmxd -c</code> kann man sich nun zu ihm verbinden und Informationen entlocken (siehe [[#Nützliches]]).

Geht alles glatt, legt er seine Routen in den Tabellen 64 & 65 ab. Das bedeuetet, dass ein einfacher ''ip r''-Aufruf keine Meshadressen anzeigt. Erst ''ip route show table all'' bringt sie zum Vorschein. Diese Routing Tabellen werden bevorzugt, da bmxd entsprechende ''ip rule''-Regeln mit höherer Präferenz als der default-Tabelle anlegt.

=== Nützliches ===

;Ausführliche Hilfe:
:<pre>bmxd -x</pre>
;Aktuelle Routen
:<pre>bmxd -cid8</pre>
;Aktuelle empfangene HNAs (''Host and Network Association'', Subnetze)
:<pre>bmxd -c --hnas</pre>
;HNA ankündigen
:<pre>bmxd -c -a 172.22.99.0/24</pre>

== Wiki ==
* [[:Kategorie:Freifunk Dresden]]

== Siehe auch ==
* [[wikipedia:de:Freifunk]]
* [https://wiki.freifunk.net/Freifunk_Dresden freifunk:Freifunk Dresden]
* Planung zu [[Datenspuren 2014/Freifunk Dresden | Freifunk bei den Datenspuren 2014]]
* [[Freifunk Dresden]] bei den [[Datenspuren]]
** [[Datenspuren 2013]]: [https://datenspuren.de/2013/fahrplan/events/5175.html Vortrag ''ddmesh''] (by [[user:W01f|vv01f]]), [http://ftp.c3d2.de/datenspuren/2013/5175_ddmesh_hq.mp4 Video]
** [[Datenspuren 2014]]: [http://datenspuren.de/2014/fahrplan.html#13k Vortrag ''Sachstand Freifunk Dresden''] (by [[user:Emploi|Emploi]])

== Weblinks ==
* [https://www.freifunk-dresden.de/ <code>www.freifunk-dresden.de</code>, Homepage von] [[Freifunk Dresden]]
** [https://dresden.freifunk.net/ <code>dresden.freifunk.net</code>]
** [http://wiki.freifunk-dresden.de/ <code>wiki.freifunk-dresden.de</code>, Wiki von] [[Freifunk Dresden]]
** [http://bt.freifunk-dresden.de/ Bugtracker von] [[Freifunk Dresden]]
* [http://www.ddmesh.de/ <code>www.ddmesh.de</code>] (wie auch <code>www.freifunk-dresden.de</code>)
** [http://download.ddmesh.de/firmware/ Firmware]
*** [http://download.ddmesh.de/firmware/latest/ar71xx/changelog.txt changelog der aktuellen Version der Firmware]
** [http://www.ddmesh.de/hotspots.html Hotspot-Liste]
* [https://github.com/ddmesh/firmware-freifunk-dresden Firmware auf GitHub]
* [https://drive.google.com/?ddrp=1#folders/0B1LY99qDqRVPdTBNT2JEMlRkRzQ Flyer / Sticker / Kollaboration für Werbemittel]

== Einzelnachweise ==
<references/>

[[Kategorie:Projekt]]
[[Kategorie:Dresden]]
