[[Kategorie:Themenabend]][[Kategorie:2004]]
{{Themenabend|
TITEL= Webseiten Sicherheit |
UNTERTITEL= Einführung zum Thema XSS und SQL-Injection|
TERMIN= 16. Januar 2004 (MKZ),7. März 2004 (5. CLT) |
ORT= [[Medienkulturzentrum Pentacon]], 5. CLT|
THEMA=  |

REFERENTEN= [[fukami]]|
C3D2WEB= ??? |
TOPIC= |
SLIDES=|
}}
==Ankündigung==
Webseiten erstellen ist mittlerweile für fast jeden Thema, der Computer, Internet und das World Wide Web im Alltag zum Austausch von Informationen nutzt. Die Webseiten werden oftmals mit Hilfe von Scriptsprachen wie PHP, Perl, JSP, ASP und wie sie nicht alle heissen mögen, sowie mit Datenbankunterstützung entwickelt. Aber egal welche dieser Sprachen zum Einsatz kommt, ob die Seiten von Profis oder von Laien programmiert wurden, oder die Seiten zu Firmen, Parteien oder Privatpersonen gehören: In allen Fällen kann es vorkommen, dass diese zu anderen Dingen missbraucht werden als ihrer eigentlichen Bestimmung. Erschwerend kommt hinzu, dass client-seitig auf vielen Sites aktives Scripting (z.B. JavaScript) aktiviert sein muss und Client-Anwendungen, die mit HTML umgehen (Browser, Mailreader) ebenfalls Probleme aufweisen. Damit ergibt sich ein Miﬂbrauchspotential, welches leidlich ausgenutzt wird.

Bei dem Vortrag wird es um Fehler gehen, die so oder so ähnlich sicher jeder schon einmal gemacht hat und gezeigt, wie diese Fehler konkret exploitet werden können. Im Einzelnen wird ausserdem der Frage nachgegangen, was sich hinter XSS-Techniken (Cross-Site-Scripting), SQL-, File- und Code-Injection verbirgt und mit leicht verständlichen Code-Beispielen erläutert. Desweiteren werden grundsätzliche Tipps und Ideen zum Absichern von Webseiten gegeben und diskutiert. 

Eingeladen sind wie immer alle selbst denkenden Wesen. 

==Gliederung==
* Einführung
* Motivation für den Vortrag
* Ein Blick auf dynamische Webseiten-Erzeugung
* Die Elemente
** HTML (a, div, style, form, iframe, script, applet, embed, object, ...)
**JavaScript (document.*, ...
** PHP: 
*** Code Execution: require(), include(), eval(), preg_replace()
*** Command Execution: exec(), passthru(), `` (backticks), system(), popen()
*** File Disclosure: fopen(), readfile(), file()
** SQL
* Techniken erläutert
** Fehler erzeugen, lesen und verstehen
** Directory Surfing, Pfad-Attacken und Common File Checking (z.B. Webserver-Pfade/Dateien, Logs, Backups, Hashes)
** Parameter-Manipulation
** Cookie Stealing und Session Hijacking
** SQL-Injection
** URL-Spoofing
** Frame Attacks mit JavaScript
* Szenarien
** Social Engineering zum Zugriff auf interne Daten
** Hijacken eines Webmailaccounts
** XSS im Zusammenhang mit Spam
* grunds‰tzliche Tips und Tricks
** webseitigen Input immer prüfen!
** nur erwünschte Zeichen zulassen
** intelligentes Error-Handling
** statisch machen was statisch geht
** auf Updates bei Scripten aus dem Web achten
** Extensions umbiegen
** RewriteRules
** Input Filter
** Dev-Kommentare löschen
* Grundschutz
** Webserver
*** immer schˆn p‰tchen!
*** weniger -v
*** default Files umbenennen oder lˆschen
*** deaktivieren von TRACE
*** Pfad Browsing unmˆglich machen (notfalls handgemacht; .htaccess)
** PHP Setup
*** register_globals aus !!!
*** safe_mode aktivieren
*** display_errors aus, log_errors an
*** expose_php 
*** sql.safe_mode
*** mysql.trace_mode
*** open_basedir setzen
*** allow_url_fopen aus
*  Client-Side (Browser)
* Audit-Werkzeuge
* Referenzen und Links

==deutsche Texte==
* [http://www.heise.de/security/artikel/38658/0 Torwandschießen] (heisec Artikel)
* [http://www.heise.de/security/artikel/43175 Giftspritze] (heisec Artikel)
* [http://www.tu-chemnitz.de/linux/tag/lt5/vortraege/folien/xss-folien.pdf XSS for fun and profit] (PDF; Folien von Stefan Krechers CLT-Vortrag)
* [[http://www.bsi.de/literat/studien/sistudien/Apache_2003.pdf Apache Webserver  - Sicherheitsstudie]] (PDF; vom Bundesamt für Sicherheit in der Informationstechnik, BSI)
* [[http://www.bsi.de/literat/studien/sistudien/IIS_2003.pdf Microsoft Internet Information Server - Sicherheitstudie] (PDF; vom Bundesamt für Sicherheit in der Informationstechnik, BSI)

==FAQs==
* [http://face2interface.com/MYD/Developer_Tips/Cross_Site_Scripting.shtml XSS FAQ] (face2interface/cgisecurity)
* [http://neworder.box.sk/newsread.php?sid=&newsid=4511 The Cross Site Scripting FAQ] (zeno, neworder)

==Papers und Tutorials==
* [http://www.cert.org/tech_tips/malicious_code_mitigation.html CERT: Understanding Malicious Content Mitigation for Web Developers] (CERT Coordination Center)
* [[http://www.cert.org/tech_tips/cgi_metacharacters.html How To Remove Meta-characters From User-Supplied Data In CGI Scripts]  (CERT Coordination Center)
* [http://www.dwheeler.com/secure-programs/Secure-Programs-HOWTO/cross-site-malicious-content.html Prevent Cross-Site (XSS) Malicious Content] (David A. Wheeler)
* [http://www.technicalinfo.net/papers/CSS.html HTML Code Injection and Cross-site scripting] (Gunter Ollmann)
* [http://www.technicalinfo.net/papers/URLEmbeddedAttacks.html URL Embedded Attacks] (Gunter Ollmann)
* [http://pgsit.org/pages/2003/gzuchlinski/libox/xss_anatomy.pdf The Anatomy of Cross Site Scripting] (PDF; Gavin Zuchlinski; alternativ http://libox.net/)
* [http://pgsit.org/pages/2003/gzuchlinski/libox/AdvancedXSS.pdf Advanced cross site scripting and client automation] (PDF; Gavin Zuchlinski; alternativ http://libox.net/)
* [http://pgsit.org/pages/2003/gzuchlinski/libox/websecdocs/XSS.pdf The Evolution of Cross-Site Scripting Attacks] (PDF; David Endler, idefense Labs )
* [http://cert.uni-stuttgart.de/archive/bugtraq/2002/05/msg00096.html Gobbles Bugtraq Posting]
* [http://pgsit.org/pages/2003/gzuchlinski/libox/websecdocs/WhitepaperSQLInjection.pdf SQL Injection] (PDF; Kevin Spett, SpiLabs)
* [http://pgsit.org/pages/2003/gzuchlinski/libox/websecdocs/Blind_SQLInjection.pdf Blind SQL-Injection] (PDF; Kevin Spett, SpiLabs)
* [http://www.cgisecurity.com/whitehat-mirror/WH-WhitePaper_XST_ebook.pdf Cross-Site Tracing] (PDF; Jeremiah Grossman, WhitehatSecurity)
* [http://pgsit.org/pages/2003/gzuchlinski/libox/websecdocs/php-security Exploiting Common Vulnerabilities in PHP Applications] (Shaun Clowes, SecureReality)
* [http://pgsit.org/pages/2003/gzuchlinski/libox/websecdocs/perl-security Security Issues in Perl Scripts] (Jordan Dimov)
* [http://pgsit.org/pages/2003/gzuchlinski/libox/websecdocs/rfp-perlcgi.txt Perl CGI problems] (rain forrest puppy)
* [http://net-square.com/papers/one_way/one_way.html One-way Webhacking]
* [http://b0iler.eyeonsecurity.org/tutorials/javascript.htm Hacking with Javascript] (b0iler)
* [http://developer.apple.com/internet/javascript/iframe.html Remote Scripting with Javascript] (Apple)
* [http://www.linux-magazin.de/Artikel/ausgabe/2004/10/php/php.html Linux Magazin: Airbag fuer den Webserver]

==Audit Tools==
* [http://www.cirt.net/code/nikto.shtml Nikto] (Perlscript das auf 2600 CGI und Files testet. Sollte nicht ohne Nachfrage beim Serveradmin getetest werden!)
* [http://gunzip.project-hack.org/webfuzz.php webfuzzer] (Security Tool zum Testen auf XSS-Möglichkeiten und Injections. auch sehr noisy!)
