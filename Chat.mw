= jabber =

Seit dem Putsch einiger Jabber-Nerds und dem Wechsel unseres hauptsaechlichen Chats auf einen Jabber-MUC-Channel besitzen wir eine eigene Jabber-Infrastruktur.

Zu unserem Multi-User Chat haben wir [[web:muc.html|eine eigene Website!]]

== Setup ==

=== wetu ===
* alternative zur HQ Infrastruktur einen Prosody Jabber Server
* erreichbar unter jabber.c3d2.de und frei zum Accounts erstellen
* Multi User Chats können unter chat.c3d2.de erstellt werden
* unter MUC: c3d2@chat.c3d2.de ist der c3d2 zu finden

== Usage ==

=== wetu ===
* Jabber-IDs: @jabber.c3d2.de
* MUC-Channel: @chat.c3d2.de
* ssl fähig

== Gimmicks ==
Ein Beweggrund fuer den Wechsel auf Jabber war der Wunsch, es Besuchern so einfach wie moeglich zu machen, mit uns zu kommunizieren. In diesem Sinne hat astro noch weitere Moeglichkeiten geschaffen, in unseren Channel zu kommen:
* littlemuc.rb, ein ruby-script, das den chat ueber <tt>ssh chat@ssh.hq.c3d2.de</tt> ermoeglicht. Das script laeuft auf [[Office2/Netz#unsafe.hq.c3d2.de|unsafe.hq.c3d2.de]].

== Technik ==

=== [[jabber.c3d2.de]] ===

==== Fingerprints ====

: 1f:e1:e9:f9:63:1d:24:8e:5a:dd:f4:8e:9f:80:14:de:a0:92:49:78
:: Aktivierungsdatum: Mon Jul 27 13:09:06 2015
:: Ablaufdatum: Sat Jan 23 12:09:06 2016

{{benötigt Dokumentation}}

…ist eine VM auf [[Wetu#jabber.c3d2.de]]. Seit dem langsamen Tod der freien Entwicklung von [[w:de:Ejabberd]], ist die VM mit [[w:de:prosody]] ausgestattet.

=== [[chat.c3d2.de]] ===
{{benötigt Dokumentation}}

== Multi-User Chat ==

=== c3d2@chat.c3d2.de ===

Der Raum ''c3d2'' auf dem Server ''[[chat.c3d2.de]]'' ist der öffentliche [[#Multi-User Chat]] vom [[C3D2]].

Dem Raum selbstverständlich mit einem Account für [[XMPP]] (aka [[jabber]]) beigetreten werden.<br />
Darüber hinaus gibt es aber auch die Möglichkeit anonym den Raum zu betreten. Dies kann beispielsweise per [[#web user interface für den Multi-User Chat]] geschehen. Aber es existiert auch [[anonxmpp.hq.c3d2.de]].

Insbesondere ist auch ''Ulf'' im MUC anzutreffen, wenn er nicht gerade wieder arg [[hacking|hackt]]. Üblicher Weise kann seine Person und seine Funktion – als richtig krasser Hacker – bei richtig krassen Fragen zu richtig krass illegalen Belangen beantworten.

Das Vergeben von Berechtigungen (wie es in einem [[#Multi-User Chat]] möglich ist<ref>https://xmpp.org/extensions/xep-0045.html#associations</ref>) erfolgt nach bewährten chaotischen Prinzip. Accounts, denen vertraut wird, erhalten die Privilegien für den Anwendungsfall ''owner'' ([http://xmpp.org/extensions/xep-0045.html#owner Owner Use Cases]). Das Gewähren von geringeren zusätzlichen Privilegien über den Anwendungsfall ''user'' ([http://xmpp.org/extensions/xep-0045.html#user Occupant Use Cases]) hinaus ist unüblich.

=== web user interface für den Multi-User Chat ===

Für Verwendung von XMPP (aka [[jabber]]) per web&nbsp;browser verwenden wir die [https://candy-chat.github.io/candy/ Software ''Candy''].

Es wird beim [[C3D2-web]] angeboten mit Candy dem [[#Multi-User Chat]] [[#c3d2@chat.c3d2.de]] beizutreten.<ref>[[web:muc.html]]</ref> Dazu kann mit einem beliebigen Nick als candy@[[jabber.c3d2.de]] dem [[#Multi-User Chat]] beigetreten werden.

== bots ==

{| class="wikitable sorable"
|-
|[xmpp:astrobot@spaceboyz.net astrobot@spaceboyz.net]
|Jabber-Interface zum [http://astroblog.spaceboyz.net/harvester/ Harvester]
|-
|[xmpp:mucbot@hq.c3d2.de mucbot@hq.c3d2.de]
|Jabber-Interface für verschiedene Dienste im [[Infrastruktur/Jabber#wetu | MUC]] [https://github.com/astro/tigger]
|-
|[xmpp:woerterbuch.info@swissjabber.org woerterbuch.info@swissjabber.org]
|Deutsch<>Englisch Wörterbuch
|-
|[xmpp:jabrss@cmeerw.net jabrss@cmeerw.net]
|Bot für beliebige RSS-Feeds
|-
|[xmpp:whois@swissjabber.org whois@swissjabber.org]
|Bot für ''whois(1)''
|-
|[xmpp:bot@jabberland.com bot@jabberland.com]
|Jabberland bot (spanisch)
|-
|[xmpp:livedelu@jobble.uaznia.net livedelu@jobble.uaznia.net]
|Bot für die [http://jobble.org/ Jobble]-Weltkarte
|-
|[xmpp:dylanbot@jabber.berlin.ccc.de dylanbot@jabber.berlin.ccc.de]
|Änderungen in http://wiki.opendylan.org/
|-
|[xmpp:postgresql@jabber.org postgresql@jabber.org]
|Bot der Links zur PostgreSQL-Dokumentation zurückliefert
|-
|}

; Siehe auch:
* [http://web.swissjabber.ch/index.php/Kategorie:Bot swissjabber.ch Kategorie:Bot]

=== pentabot ===

Der pentabot ist der bot zum [[C3D2]].

Magic prefix ist <tt>+hq</tt>, Hilfe gibt es mit <tt>+hq help</tt>.

==== FreeBOT ====

Seit 2013 gibt es den FreeBOT.

Der Code vom FreeBOT basiert dem [https://github.com/koeart/pentabot Code vom Pentabot] (von [[user:blotter|blotter]]/[[user:koeart|koeart]]).

Ergänzend gab es Anpassungen von [[Daniel]]. Der FreeBOT lief in einer Jail von FreeBSD (der Architektur SPARC64).

===== Pentabot im LXC =====

[[#FreeBOT]] wurde Anfang 2015 von [[Daniel]] so weiterentwickelt (angepasst), dass der [[#pentabot]] in einem [[LXC]] läuft. Der [[#Pentabot im LXC]] heißt jetzt wieder ''Pentabot'', obwohl er eine Weiterentwicklung von [[#FreeBOT]] ist.

====== notwendige Fixes von [[#FreeBOT]] für Pentabot im LXC ======

; python-xmpp:
: [https://raw.githubusercontent.com/freebsd/freebsd-ports/master/net-im/py-xmpppy/files/patch-xmpp-transports.py Dokumentation der notwendigen Änderungen] (durch das Projekt [[FreeBSD]])
<source lang="bash">
--- xmpp/transports.py.orig	2010-04-06 21:05:04.000000000 +0800
+++ xmpp/transports.py	2010-04-06 21:05:20.000000000 +0800
@@ -27,7 +27,7 @@ Transports are stackable so you - f.e. T
 Also exception 'error' is defined to allow capture of this module specific exceptions.
 """
 
-import socket,select,base64,dispatcher,sys
+import socket,ssl,select,base64,dispatcher,sys
 from simplexml import ustr
 from client import PlugIn
 from protocol import *
@@ -312,9 +312,9 @@ class TLS(PlugIn):
         """ Immidiatedly switch socket to TLS mode. Used internally."""
         """ Here we should switch pending_data to hint mode."""
         tcpsock=self._owner.Connection
-        tcpsock._sslObj    = socket.ssl(tcpsock._sock, None, None)
-        tcpsock._sslIssuer = tcpsock._sslObj.issuer()
-        tcpsock._sslServer = tcpsock._sslObj.server()
+        tcpsock._sslObj    = ssl.wrap_socket(tcpsock._sock, None, None)
+        tcpsock._sslIssuer = tcpsock._sslObj.getpeercert().get('issuer')
+        tcpsock._sslServer = tcpsock._sslObj.getpeercert().get('server')
         tcpsock._recv = tcpsock._sslObj.read
         tcpsock._send = tcpsock._sslObj.write
</source>

== Userpassword ändern ==

Für <tt>hacker@jabber.ccc.de</tt>:
<pre>
<iq type='set' to='jabber.ccc.de'>
  <query xmlns='jabber:iq:register'>
    <username>hacker</username>
    <password>meinneuespasswort</password>
  </query>
</iq>
</pre>

== Psi passwort recovery ==

Für den Chat client Psi geht das mit folgendem Perl 1-Zeiler:
: <code>perl -le '($jid,$pw)=@ARGV;$pw=~s/..(..)/chr hex$1/ge; print substr($pw^$jid,0,length$pw)' user@host.tld password</code>

Dabei ist
: ''user@host.tld'' die jid und
: ''password'' der string aus der <code>.psi/profiles/<profile>/config.xml</code> unter dem <password>-Feld vom betreffenden <account> .

; Zum Verständnis: jid und password werden in utf-8 Darstellung ge'xor'ed.

Quelle: http://blogmal.42.org/rev-eng/psi-password.story

= IRC =

; Kanal: <code>#c3d2</code>
; Server: <code>[http://hackint.org/ hackint.org]</code>

(Da ist kaum wer denn alle sind auf Jabber. Selbst als Fallback ist IRC ungeeignet da man andere Clients braucht. Wir wollen einen neuen MUC-Gateway der als IRC-Server erscheint, so dass wieder alle das Protokoll ihres Vertrauens nutzen und dennoch miteinander reden können.)

; Siehe auch:
* [[#Multi-User Chat]]

= Allgemein =

== Einzelnachweise ==
<references />

[[Kategorie:Infrastruktur]]
