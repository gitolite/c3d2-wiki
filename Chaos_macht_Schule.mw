== Projekt ==

[[Chaos macht Schule]], kurz [[CMS]], ist ein Projekt, das die Förderung von Medienkompetenz und Technikverständnis in Schulen zum Ziel hat. Es werden entsprechend Workshops angeboten und durchgeführt. Sie richten sich an die Beteiligten von Schule:
* Schülerinnen und Schüler,
* Lehrerinnen und Lehrern sowie
* Eltern.

== Kontakt ==

Bei Anfragen oder zum Mitmachen, bitte eine Email an: [mailto:schule@c3d2.de schule@c3d2.de].

Diskussionen und Beteiligung bei [[CMS]] erfolgt bei den gelegentlichen Treffen zur Vorbereitung und über [[lists:cms]].

== FAQ ==

Hier ein paar Dinge die regelmäßig zur Sprache kommen und wie folgt bisher beantwortet wurde. Dies kann als Anhalt für die Kommunikation mit Anfragenden genutzt werden.

=== Welche Informationen werden für die Planung von Veranstaltungen benötigt? ===

Wir bitten, aufgrund der vielseitigen Kommunikation, eine Anfrage für eine Veranstaltung bei einer Bildungseinrichtung etwas ausführlicher zu formulieren.

Sollten genauere Angaben noch nicht bekannt sein, bitten wir dennoch im eine Einschätzung.

* Wann ist die Veranstaltung geplant?
* Wo soll sie stattfinden?
* Was ist das Hauptthema und welche Punkte sollen wir beitragen? Nennen Sie ruhig konkrete Beispiele.
* Wer ist das Zielpublikum, welche Vorkenntnisse sind absehbar vorhanden?
* Wie lange ist die Veranstaltung und welcher Zeitrahmen ist für unseren Teil vorgesehen?

=== Wie hoch ist das Honorar? ===

Generell nehmen wir keine Honorare oder spenden diese womöglich an den [[Netzbiotop e. V.]] (Verein der die Räumlichkeiten des [[C3D2|Chaos Computer Club]]s finanziert. Wir sind der Meinung dass Bildung (vor allem an Schulen) unabhängig vom Geldbeutel sein soll. Da viele von uns Studentinnen und Studenten sind, halten sich Fahrtkosten auch eher im Rahmen. Das Studentische Jahresticket der (meisten, heißt min. TU und HTW Dresden) Dresdener Hochschulen gilt für den VVO und das Angebot der DB innerhalb des Freistaates Sachsen.
Wenn wir allerdings ein Honorarpreis nennen sollen, weil z.B. eine Spende aus der Einrichtung nicht möglich ist, würden wir uns entsprechend in Absprache eine Lösung überlegen.

== Veranstaltungen ==

{| class="wikitable"
|-
! Titel !! Ort !! Datum !! Themen !! Art !! Vortragende !! Bemerkungen !! Folien !! Protokoll
|-
| Umgang  mit Sozialen Netzwerken
|| Dresden (Infomatiklehrertagung)
|| 22.03.2012
||
* Internet
** Grundlagen des Internets
** Pseudonymität
** Risiken im Internet
** Praxis: Kindernet
** Entwicklungsphasen des Internets
** Internetnutzung
* Soziale Netzwerke
** Eigenschaften
** Geschäftsmodelle
** Kinderbook
** Lernziel
* Freiheit
** Was wir vermitteln wollen
** Freie Lizenzen
** Freie Medien
** Freie Software
* Fazit
|| Vortrag Lehrer
|| Marius Melzer, Paul Schwanse, Stephan Thamm
||
|| https://github.com/c3d2/informatiklehrertagung
||
|-
| Chaos macht Schule
|| Dresden (Datenspuren 2012)
|| 13.10.2012
||
* Motivation
* Praxis
** Schulbesuche
** Veranstaltungen für Lehrer
** Elternabende
** Junghackertrack
** Workshops
** Vernetzung
* Inhalte
** Herangehensweise
** Internetgrundlagen
** Kindernet
** Datenschutz
** Passwortsicherheit
** Soziale Netzwerke
** Tracking und Selbstschutz im Internet
** Freie Software, freie Lizenzen
** Löten
** Programmierung
|| Vortrag Datenspuren 2012
|| Marius Melzer, Stephan Thamm
||
|| https://github.com/c3d2/cms-datenspuren2012
||
|-
| Freie Lizenzen bei Lehr- und Lernmaterialien
|| Dresden
|| 13.03.2013
||
* Urheberrecht
* Vier Freiheiten
* Creative Commons
* Weitere freie Lizenzen
* Open Educational Ressources
* Open Courseware & Weiteres
* Wikimedia und weitere Quellen für CC Materialien
|| Vortrag Lehrer
|| Marius Melzer, Stephan Thamm, Paul Schwanse
||
|| https://www.github.com/c3d2/sit-2013
||
|-
| Soziale Netzwerke
|| Riesa
|| 21.03.2013
||
* Soziale Netzwerke
** Geschäftsmodelle
** Gefahren
** Datenschutz
|| Vortrag Schüler
|| Marius, Paul
||
|| https://github.com/c3d2/cms-riesa
||
|-
| Internet und Netzwerke
|| TU Dresden
|| 23.03.2013
||
* Internetgrundlagen
** IP, DNS
** OSI-Schichtenmodell
** HTTP, SMTP
** Verschlüsselung (symmetrisch, asymmetrisch), Signieren
** SSL, Web of Trust
** Tor
|| Workshop Studenten (MINToring Projekt)
|| Marius Melzer, Stephan Thamm
|| Hauptsächlich Praxisteil/Workshop zu Wireshark und co
|| https://github.com/c3d2/cms-mint
||
|-
| Freie Lizenzen und Lehrmaterialien
|| Chemnitz
|| 27.06.2013
||
* Einleitung: Wissen
* Creative Commons
** Übersicht
** Module
** Baukasten
* Weitere Lizenzen
* Weitere Angebote
** Open Educational Resources
** Creative Commons Search
** Bücher
** Open Courseware
* Lizensierung eigener Werke
|| Vortrag Lehrer
|| Marius, Stephan
||
|| https://github.com/c3d2/cc-lizenzen
||
|-
| Chaos macht Schule
|| Erzgebirge
|| 03.07.2013
||
* Grundlagen des Internets
* Personalisiertes Web
* Soziale Netzwerke
* Datenschutz
|| Vortrag Schüler
|| Marius Melzer, Stephan Thamm
||
|| https://github.com/c3d2/cms-erzgebirge
||
|-
| NSA, Prism & co
|| Meißen
|| 16.10.2013
||
* Was ist Sicherheit?
* Tempora
* E-Mail
* Verschlüsselung
** SSL/TLS
** GPG/PGP
* Dezentral
* PRISM
* Zensur
* Tor
* Metadaten
* Tracking
|| Vortrag Lehrer
|| Marius, Stephan
||
|| https://github.com/c3d2/cms-meissen
||
|-
| NSA,  Prism und co - Wie schützt man sich vor Überwachung
|| Internet
|| 26.11.2013
||
* Kommunikationsarten im Internet
* Wie schütze ich meinen Computer/Smartphone (Permissions, Firewalls, FOSS)
* Wie schütze ich Internetverbindungen (Verschlüsselung, SSL, Zertifikate)
* Anonymität im Internet (Metadaten, Tor, Zensur)
* Wie schütze ich mich gegenüber genutzten Internetservices (Ende-zu-Ende, Datensparsamkeit, Tracking-Gegenmaßnahmen)
|| Onlinevortrag Studenten
|| Marius
||
|| https://github.com/c3d2/cms-nsa
||
|-
| Datenschutz / soziale Netzwerke
|| Leipzig
|| 22.01. 14:00
|| 
* Datenschutz
|| Vortrag Lehrer
|| Marius, Stephan
||
|| https://github.com/c3d2/cms/tree/master/2014_01_22_datenschutz
||
|-
| Akademische Woche Afra
|| Dresden
|| 10.02.2014 14:30
||
* NSA-Stuff
|| Vortrag Schüler
|| Marius, Stephan, große Nerdgruppe
|| Große Schülergruppe
|| https://github.com/c3d2/cms/tree/master/2014_02_10_prism_und_co
||
|-
| Freie Lizenzen
|| Leipzig
|| 12.03. 13:30
|| 
* Freie Lizenzen
|| Vortrag Lehrer
||
||
|| https://github.com/c3d2/cms/tree/master/2014_03_12_freie_lizenzen
||
|-
| Schulinformatiktag<sup>[http://inf.tu-dresden.de/article.php?node_id=1&ln=de&article_id=598]</sup>
|| Dresden (Fakultät Informatik TU&nbsp;Dresden)
|| 19.03.2014
|| 
* NSA-Stuff
|| Vortrag (und Diskussionsrunde) Lehrer
|| Marius, Stephan, Stefan
|| 
|| https://github.com/c3d2/cms/tree/master/2014_03_19_schulinformatiktag
||
|-
| Elternabend
|| Dresden
|| 24.03. 18:30
|| 
|| Elternabend
|| Bernhard?
|| 
|| https://github.com/c3d2/cms/tree/master/2014_03_24_elternabend
||
|-
|
|| Bischofswerda
|| 25.03.2014 14:00
||
|| Vortrag Lehrer
||
||
* Kontakt: Marius
* Regionale Fachkonferenz Bischofswerda
||https://github.com/c3d2/cms/tree/master/2014_03_25_bischofswerda
||
|-
| Girls Day
|| Dresden (TUD - Info-Fakultät)
|| 27.03. 09:00 - 11:00
|| 
* Pentabugs
|| Workshop Schüler
|| bigalex, Stephan, ?
|| max. 10 Teilnehmerinnen
||
|-
| Projekttag
|| Dresden
|| 07.04. 11:00 - 12:30
|| 
* Datenschutz
* ...
|| Vortrag Schüler
||
||
|| https://github.com/c3d2/cms/tree/master/2014_04_07_dresden
||
|-
| Das Private und das Öffentliche
|| Ringvorlesung der polit. Hochschulgruppen Dresden
|| 23.04.2014, 16:40h
||
* ???
* NSA
* Datenschutz, Datensicherheit, Datensparsamkeit
* Private Daten im Internet
|| Vortrag Studenten
||
||
||
||
|-
| FLURGESPRÄCHE_25: How to disappear
|| TU Dresden - Kunstpädagogik
|| 20.05.2014, 18:30h
||
* NSA
* Metadaten
* Datenschutz, Datensicherheit, Datensparsamkeit
* Verschlüsselung
* Einrichtung verschiedener Browser-Plugins, Tor-Bundle und Mail-Verschlüsselung bei Teilnehmern
|| Vortrag / Workshop Studenten
|| Stefan, Martin, Marius
||
|| https://github.com/c3d2/cms/tree/master/2014_05_20_flurgespraeche
||
|-
| Sukuma Award 2014 "Faire Elektronik" / "Faire IT"
|| HQ?
|| 27.05.2014 bis 20.07.2014
||
* Freie Software
|| Vortrag
|| ?
||
|| ?
||
|-
| Bahratal
|| Bahratal
|| 23.07.2014
||
* NSA
* Metadaten
* Verschlüsselung
* Datenschutz, Datensicherheit
|| Vortrag / Workshop FJSler
|| Marius, Stephan
||
|| https://github.com/c3d2/cms/tree/master/2014_07_23_bahratal
||
|-
| Az Conni
|| Az Conni, Dresden
|| 20.08.2014
||
* NSA
* Metadaten
* Datenschutz, Datensicherheit, Datensparsamkeit
* Verschlüsselung
|| Vortrag
|| Stefan, Martin
||
|| https://github.com/c3d2/cms/tree/master/2014_08_20_az_conni
|| https://pentapad.c3d2.de/p/cms-20082014
|-
| Datenspuren 2014
|| Scheune, Dresden
|| 13./14.09.2014
||
* NSA
* Metadaten
* Datenschutz, Datensicherheit, Datensparsamkeit
* Verschlüsselung
|| Vortrag und Infostand ("Digitale Selbstverteidigung")
|| Martin, Stefan
||
|| https://github.com/c3d2/cms/tree/master/2014_09_13_datenspuren
|| https://pentapad.c3d2.de/p/cms-2014-09-08
|-
| Projekttag an 62. Oberschule
|| 62. Oberschule (Dresden)
|| 14.10.2014 8:30 Uhr
||
* Datenschutz, Datensicherheit, Datensparsamkeit
* Soziale Netzwerke
* Verschlüsselung
|| Vortrag/Workshop
|| Stephan, Stefan
||
|| https://github.com/c3d2/cms/tree/master/2014_10_14_projekttag_62os
|| https://pentapad.c3d2.de/p/cms-62os
|-
| Medienfestival 2014
|| Technische Sammlungen, Dresden
|| 15.11.2014 / 16.11.2014
||
* Vortrag (Datenschutz, Datensicherheit, Datensparsamkeit, Verschlüsselung)
* Pentabugs
|| Vortrag/Workshop
|| Stephan, Stefan, koeart, bigalex
||
|| https://github.com/c3d2/cms/tree/master/2014_11_15_abschluss_cmt
|| 
|-
| Schulinformatiktag 2015
|| TU Dresden, Fakultät Informatik
|| 18.03.2015
|| * Smartphones und Datenschutz
|| Vortrag/Workshop
|| Stephan, koeart
||
|| https://github.com/c3d2/cms/tree/master/2015_03_18_sit21
|| 
|-
| Datenspuren 2015
|| Technische Sammlungen, Dresden
|| 19.06.2015
||
* Tempora
* Prism
* Metadaten (Fokus)
|| Vortrag/Workshop
|| Stephan
||
|| https://github.com/c3d2/cms/tree/master/2015_06_19_datenspuren
||
|-
| Musikgymnasium
|| Dresden
|| 25.08.2015
||
* Verschlüsselung
* Metadaten/Anonymität
* Soziale Netzwerke
|| Vortrag/Workshop
|| koeart, Marius
||
|| https://github.com/c3d2/cms/tree/master/2015_08_25_musikgymnasium
||
|-
| Medienfestival
|| Dresden
|| 15.11.2015
|| "2 Jahre nach Snowden"
* Tempora/Prism
* Landesverrat, NSAUA, VDS
|| Vortrag/Workshop
|| Stephan
||
|| https://github.com/c3d2/cms/tree/master/2015_11_15_medienfestival
||
|-
| AK Digitale Medien, Landespräventionsrat
|| Dresden
|| 23.11.2015
||
* Metadaten
* Backdoors
* Verschlüsselung
* Tor
* Dezentrale Dienste
* FOSS
|| Vortrag/Workshop
|| Marius
||
|| https://github.com/c3d2/cms/tree/master/2015_11_23_landerpraeventionsrat
||
|-
| FSJler Politik
|| Leipzig
|| 15.12.2015
||
* Backdoors -> FOSS
* Verschlüsselung
* Dezentrale Dienste
* Metadaten -> Tor
|| Vortrag/Workshop
|| Marius
||
|| https://github.com/c3d2/cms/tree/master/2015_12_15_leipzig_fsjler
||
|-
| colspan=8 style="text-align: center" | '''Zukunft'''
|-
! Titel !! Ort !! Datum !! Themen !! Art !! Vortragende !! Bemerkungen !! Folien !! Protokoll
|-
|}

== Material ==
* http://soup.chaossource.net/post/385601644/metadata-matters
* http://www.spiegel.de/netzwelt/web/facebook-studie-likes-enthuellen-persoenlichkeit-a-888151.html

[[{{ns:image}}:cms-text.png]]

== Siehe auch ==
* [[web:schule.html]]
* [[ftp:/misc/cms/]]
* https://ccc.de/schule

[[Kategorie:Chaos macht Schule]]
