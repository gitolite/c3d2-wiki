[[Spende]]n an den [[C3D2]] richten sich an das [[Netzbiotop]], als den [[Verein]].

== Gemeinnützigkeit ==

Feststellung: Der Netzbiotop e.&nbsp;V. ist seit Gründung am 30.08.2005 vom Finanzamt Dresden-Süd (aka Finanzamt Dresden 1) als gemeinnützig anerkannt.

Das Datum des letzten Freistellungsbescheides ist der 25.10.2013.

Die Ausstellung von Spendenquittungen (genauer: [[#Zuwendungsbestätigungen]]) erfolgt durch den [[Vorstand]] ([mailto:vorstand@c3d2.de vorstand@c3d2.de]).

Es gibt folgende zwei wichtige Bescheinigungen zur Absicherung der Gemeinnützigkeit<ref>http://www.nonprofit.de/artikel-lesen/artikel/gemeinnuetzigkeit-bescheinigung/</ref>:
* [[#Vorläufige Bescheinigung der Gemeinnützigkeit]]
* [[#Bescheinigung der Gemeinnützigkeit]]

=== Vorläufige Bescheinigung der Gemeinnützigkeit ===

Entspricht die Satzung den gemeinnützigkeitsrechtlichen Voraussetzungen, erteilt das Finanzamt bei Neuanträgen zunächst eine vorläufige Bescheinigung. Eine solche Bescheinigung hat grundsätzlich die Gültigkeit von 18&nbsp;Monate. Für Spendenzwecke gilt Eine solche Bescheinigung 3&nbsp;Jahre ab Ausstellungsdatum.

=== Bescheinigung der Gemeinnützigkeit ===
: oder Freistellungsbescheid

Nach 18&nbsp;Monaten wird der Verein aufgefordert, eine Gemeinnützigkeitserklärung abzugeben, anhand der das Finanzamt überprüft, ob der Verein auch tatsächlich die Voraussetzungen der Gemeinnützigkeit erfüllt. Sind diese Voraussetzungen gegeben, wird dem Verein ein Freistellungsbescheid erteilt, der für Spendenzweck 5&nbsp;Jahre Gültigkeit ab dem Ausstellungsdatum hat. Nach Erteilung des Freistellungsbescheides wird der Verein turnusmäßig alle 3&nbsp;Jahre überprüft.

Wird der Verein zur Körperschaftssteuer veranlagt und erhält er statt eines Freistellungsbescheides einen Körperschaftssteuerbescheid, bescheinigt das Finanzamt die Gemeinnützigkeit in Form einer Anlage zum Körperschaftssteuerbescheid. Der Verein hat seiner Bank in Schriftform mitzuteilen, ob z. B. die Kapitalerträge im steuerfreien oder steuerpflichtigen Bereich anfallen werden. Hinsichtlich der Spendenzwecke hat der Körperschaftssteuerbescheid 5&nbsp;Jahre Gültigkeit ab dem Ausstellungsdatum.

== Zuwendungsbestätigungen ==

=== Verfahren für Zuwendungsbestätigungen ===

Offizielle Vorlagen gibt es bei der [https://www.formulare-bfinv.de/ffw/content.do Bundesfinanzverwaltung]:
* [https://www.formulare-bfinv.de/ffw/catalog/openForm.do?path=catalog%3A%2F%2FSteuerformulare%2Fgemein%2F034122 010 - Bestätigung über Geldzuwendung / steuerbegünstigte Einrichtung / Verein010 - Bestätigung über Geldzuwendung / steuerbegünstigte Einrichtung / Verein]
* [https://www.formulare-bfinv.de/ffw/catalog/openForm.do?path=catalog%3A%2F%2FSteuerformulare%2Fgemein%2F034123 015 - Bestätigung über Sachzuwendung / steuerbegünstigte Einrichtung / Verein015 - Bestätigung über Sachzuwendung / steuerbegünstigte Einrichtung / Verein]

Wünschenswert wären eine Zuwendungsbestätigung, angepasst direkt vom Verein auf der Website;
: Entwurf wird erarbeitet: [[git:c3d2-web/tree/draft/latex?h=netzbiotop]]

Bisher muss eine Spenderin die Bestätigung vom Vorstand ([mailto:vorstand@c3d2.de vorstand@c3d2.de]) anfordern, gem. Festlegung durch den Vorstand. Denkbar wäre eine spätere Festlegung/Ergänzung z.B. in der [[Intern:Satzung/Kassenordnung|Kassenordnung]].

=== Absicht zu Zuwendungsbestätigungen ===

Es soll ein Formular für die Bestätigung der [[Spende]]n bereitgestellt werden.

Die Idee ist, dass Spenderinnen damit den Vorgang der Ausstellung beschleunigen können. Gegebenenfalls können weitere Vereine/Körperschaften die bereitgestellten Ressourcen auch nutzen.

=== ToDo zu Zuwendungsbestätigungen ===

* Finden: Vorlage f. "Bestätigung über Sachzuwendung/Geldzuwendung, steuerbegünstigte Einrichtung/Verein" (Kandidaten: [http://www.vereinsbesteuerung.info/spendenbesch.htm 1][https://github.com/vv01f/spendenquittungen-mit-latex 2][http://amt24.sachsen.de/ZFinder/verfahren.do?action=showdetail&modul=VB&id=402581!0 3] [https://www.formulare-bfinv.de/ffw/content.do 4]) 
* Anpassung: ggf. Format befreien und "branden"
* Prüfung: durch das [[Finanzamt]] bestätigen lassen ("Ein Muster Ihrer Spendenbescheinigung können Sie Ihrem Finanzamt zur Überprüfung zukommen lassen."<ref>http://www.vereinsbesteuerung.info/spendenbesch.htm</ref>)

== Siehe auch ==
* [[Finanzen#Arten von Finanzierung]]

== Einzelnachweise ==
<references />

[[Kategorie:Finanzen]]
[[Kategorie:Verein]]
