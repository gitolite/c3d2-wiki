= Datensicherheit =

== Fragestellung ==
Was ist Datensicherheit?

== Mehrere Aspekte ==
Datensicherheit, allgemeiner Informationssicherheit ist ein Begriff, das sich aus verschiedenen Perspektiven anders darstellt und verschiedene Lösungen benötigt. Zumeist werden die technischen Lösungen bevorzugt behandelt, dies ist aber bei sozialen Problemstellungen meist nicht hinreichend. Beim näherer Betrachtung fällt auf, dass Datensicherheit mindestens die folgenden Aspekte betrifft:
* Integrität
* Vertraulichkeit
* Authentizität
* Verfügbarkeit

== Aus verschiedenen Perspektiven ==
Wir haben uns dem Begriff von mehreren Anwendungen ausgehend genähert.

=== Datenübertragung ===
Eine Nachricht soll im Sinne der oben benannten Punkte von Alice zu Bob.
Welche Verfahren können helfen diese Aspekte zu sichern bzw. was bedeuten diese Punkte im Zusammenhang z.B. einer E-Mail.

==== Integrität ====
Die E-Mail soll ihren Inhalt nicht verändern. Nicht ganz. Die E-Mail soll sich nicht verändern.

Dateien sind auf bestimmte Art enkodiert, würde sich ihr Inhalt auf dem Weg von Alice nach Bob ändern, könnte sie bei Bob eventuell nicht korrekt dargestellt werden oder auch nicht zugestellt werden. Abstrahiert man die E-Mail und betrachtet nun beliebige Daten trifft man bei der Übermittlung von Daten häufig auf diese Anforderung.
Dafür haben sich Prüfsummen, bei der Übertragung von digitalen Daten, oft als günstig erwiesen und sind auf jeder Ebene der Datenvermittlung zu finden.

Eine andere Lösung ist die Nutzung vertrauter Vermittler. Also jenen denen man zu mutet, dass sie das zu Übermittelnde nicht verändern.

Da nun bei der Übertragung von Daten über das Internet viele verschiedene Komponenten verschiedenster Hersteller auf dem Weg von Alice zu Bob beteiligt sind und die eigentlich Nachricht oder Datei das Encoding auf diesem wechselt, ist diese Praxis glücklicher Weise nicht gängig.

Der Datenstrom wir also mehrfach auf seinem Weg auf Integrität geprüft.

===== Man in the Middle =====
Wenn auf dem Weg von Alice zu Bob, Nina die zu übertragenen Daten abfängt und sie verändert, möge man diese Attacke Nina und ihren Hilfsmitteln vorwerfen. Die ganzen Router, Switches und Netzwerkkarten, die alle fleißig ihre Prüfsummen bilden, können ja nicht ahnen, dass sie von Nina so missbräuchlich benutzt werden. Die Integrität der Nachrichten, von Alice zu Nina und von Nina zu Bob als einzelne Nachrichten betrachtet, wird durch sie nicht beeinflusst.

Oder?

Auf Grund eben jener Zweifel drängen wir auf die Offenlegung der genutzten Verfahren und Geräte.

===== Prüfsumme auf den Inhalt =====
Angenommen es gäbe für den Inhalt eine so für Nina so aufwendig zu fälschende Prüfsumme, dass Nina den Aufwand scheut, kann sich Alice glücklich schätzen.

Ein offenes Verfahren dafür bietet GnuPG.

Hier wird der Inhalt mit einem Zertifikat versehen, das auch Signatur genannt wird, und der Nachricht beigelegt. Das Verb "versehen" abstrahiert hier bei ein Verfahren bei dem der Inhalt und eine recht zufällige Zahl, die einer Identität zu geordnet wurde, zu dem beigefügten Zertifikat fälschungssicher verrechnet wird.

==== Vertraulichkeit ====
Da es nun offensichtlich möglich ist, einen Datenstrom abzufangen, kann es für nötig erachtet werden, Dritten die Möglichkeit des Mitlesens, durch Kryptografie so zu erschweren, dass das Brechen dieser Kryptografie länger braucht, als die übermittelten Daten von Interesse sind.

Der zu versendende Text soll hier für nur eine bestimmte Emfängeridentität entschlüsselbare Form umgewandelt werden.

Hier bei sollte klar sein, dass kein Verschlüsselungsverfahren ultimativ ist. Es ist herrscht ein reges Tauziehen zwischen jenen Kräften, die kryptografische Verfahren entwickeln und brechen.

==== Authentizität ====
Es bei der Übermittlung von Daten häufig nicht nur wichtig was übermittelt wurde, sondern ebenso von wem diese Daten stammen.

Ein weiterer Grund, von dem aus es sinnvoll ist eine Prüfsumme mit Identitätsbezug über den Inhalt einer Nachricht zu bilden. Die Reputation der Quelle ist unabhängig der übertragenen Daten.

===== Absenderkennung =====
Im Falle von E-Mail wird häufig die Absenderkennung als alleiniger Indikator für die Quelle einer Nachricht genutzt. Diese kann jedoch gerade im Falle einer E-Mail beliebig Gesetzt werden.

Das ist auch ganz okay, da nicht jede Nachricht, die via E-Mail versandt wird, von einer real existenten oder verkörperten Identität stammen muss, man Nachrichten jedoch nach diesem Kriterium sortieren, klassifizieren und suchen kann.

Möchte man jedoch wirklich sicher gehen, dass die empfangene Nachricht eine valide Quelle hat, sollte man allerdings eine über den Inhalte gebildete Prüfsumme mit Identitätsbezug der Absenderkennung vorziehen.

==== Verfügbarkeit ====
Im Zusammenhang mit der Datenübertragung spielt dieser Aspekt nicht mit hinein, da wir davon ausgehen, dass die Daten, die übermittelt werden sollen, auch zur Verfügung stehen.


=== Datenhaltung ===
Wir übertragen und erzeugen Daten sicher auch aus Vergnügen, allerdings wohl meistens eher aus dem Grund, dass wir sie für spätere Verwendung vorhalten wollen.

==== Integrität ====
Gespeicherte Daten sollen sich nach dem Ablegen unter einer Referenz nicht verändern.

Verschiedene Technologien wurden für diesen Zweck entwickelt. Von Steintafeln über Papyrus, Schriftrollen und das Buch bis hin zu modernen Speichermedien, wie Festplatten.
Wie die Geschichte zeigt, ist keine dieser Lösungen perfekt und es ist eher eine Sache der regelmäßigen Instandhaltung des Datenträgers.
Es ist also den physikalischen Eigenschaften des Datenträgers zu verdanken, wenn die dort abgelegten Daten sich nicht über die Zeit verändern.

Gleichzeitig steht der Begriff Integrität im Zusammenhang mit Datenhaltung auch für Widerspruchsfreiheit. So wird Bob heute wohl kaum seinen 90. Geburtstag begehen, wenn er doch erst in 4 Monaten das Licht der Welt erblickt. Modernen Datenbankmanagmentsystemen kann man darum Regeln zur Integritätssicherung der Daten einverleiben.

==== Authentizität ====
Oft als Bestandteil der Integrität betrachtet. Möchte man die Herkunft eines Datensatzes zur Beurteilung der Korrektheit heranziehen, so ist es nötig, dass sich die Quelle gegenüber dem Datenhaushalt authentifiziert. Daten sollen also nur von beglaubigten Quellen abgelegt bzw. verändert werden können.

==== Vertraulichkeit ====
Da, wohl möglich, nicht jeder abgelegte Datensatz für jeden einsehbar sein soll, spielt das kryptografische sichern von Daten auch bei der Datenhaltung eine Rolle. Die Daten sollten nur jenen zugängig sein, die auch dazu berechtigt sind.

==== Verfügbarkeit ====
Damit beim Verlust oder Defekt eines Datenträgers, die Daten weiterhin mit gewisser Sicherheit zur Verfügung stehen, ist kann es nötig sein, sie auf einem weiteren Datenträger zu halten. Redundanz im Umgang mit digitalen Daten wird durch Verfahren wie RAID und/oder Sicherungskopien erzeugt. Aber auch moderne Dateisysteme bieten einem Möglichkeiten redundant Daten zu halten.

==== Abstreitbarkeit ====
Bei unbefugtem Zugriff auf Datenspeicher kann es erwünscht sein, die Herkunft oder sogar den Besitz von Daten nicht ungewollt nachweisbar zu machen. Dies wird z.B. bei TrueCrypt eingesetzt: Mehrere Volumes können in einem Container parallel bereit gehalten werden. Der Zugriff auf jedes Volume ist durch einen anderen Schlüssel gesichert und die Existenz weiterer Volumes ist nicht nachweisbar. Dieser Ansatz hat auch zum Nachteil, dass die Nicht-Existenz von Daten nicht (durch Vorzeigen) bewiesen werden kann.
