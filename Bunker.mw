{{historisch}}

Zur Erläuterung:
* am 26.10.2011 teilte Herr Hütt vom Amt für Brand- und Katastrophenschutz mir (--[[Benutzer:Hakunamenta|Hakunamenta]] 09:24, 26. Okt 2011 (UTC)) mit das alle drei zur Zeit in Dresden befindlichen Bunker dem Amt für Brand- und Katastrophenschutz zur _ausschließlichen_ Nutzung für den Katastrophenschutz zu Verfügung gestellt werden.  Das heißt im Klartext das die Dinger mit Sandsäcken gefüllt sind.  Weiterhin führte er aus das es für die Bunker schon seit geraumer Zeit weder Wasser- noch Abwasseranschluss gibt.
Ich denke damit ist die Sache erledigt.  No bunker for HQ, sry guys.

----

Im folgenden der alte Artikel:

Im folgenden Bereich findet ihr den derzeitigen Informationsstand zum Thema HQ 2.0 (Bunker-Projekt).

Mit "Ich" ist Accuso gemeint, von dem der Text im Wesentlichen stammt.

=Planung=
== Allgemein ==

Der Bunker befindet sich in der Hans-Dankner-Straße [[http://maps.google.com/maps?q=51.041939+13.738372]] und ist ein SBW-300 mit 220m² Grundfläche. Der Grundriss auf [[http://www.sachsenschiene.net/bunker/bun/bun_73.htm]] stimmt nicht mit der tatsächlichen Situation überein, gibt aber immerhin einen groben Überblick. Die Abmessungen der Seitennischen sind 4,2m x 2,95m.

Um die Frage mit den Abluftsystem zu beantworten (Protokoll):
* 4 Lüfter * 2600 Liter/(Lüfter*Minute)

== Zugang ==

Wie ihr bereits lesen konntet (oder im c3d2 gehört habt), ist uns der Zugang derzeit mangels einem Mietvertrag noch verwehrt. Das liegt weder an unserem Unwillen noch an internen Problemen unsererseits, sondern schlicht an der Unfähigkeit der Ämter (anfänglich war der Zuständigkeitsfrage das größte Problem). Wir könnten natürlich sofort mieten und uns dann ein Bild davon machen, wie ihr alle aber mit Sicherheit nachvollziehen könnt, ist mir wenig daran gelegen, die Katze im Sack zu mieten, weshalb ich allem voran eine Besichtigung im Beisein der Mitglieder des Netzbiotops und natürlich sämtlicher Interessenten im c3d2 haben will, bevor wir mieten.

Leider waren unsere Bemühungen derzeit noch nicht von Erfolg gekrönt, aber es hat sich ein hoffentlich kompetenter Ansprechpartner gefunden, auf den wir uns mehr verlassen können als auf innerstädtische Ämter.

Demnach folgen die Grundbedingungen, die im Mietvertrag/Pachtvertrag mit absoluter Sicherheit auftauchen werden, um euch einen Überblick über unsere neuen Pflichten geben zu können:

- Wir werden für 1 € im Jahr mieten (die Stadt spart sich dadurch fast 5300 € an Gebühren und Personalkosten zur Instandhaltung und Kontrolle des Bunkers). Die Pflichten dessen gehen unmittelbar an uns über und MÜSSEN eingehalten werden. Wir werden in halbjährlichen Abständen vom Amt für Katastrophenschutz (das die Instandhaltung etc. kontrolliert) unangemeldeten Besuch bekommen.

- Die Stadt wird uns alle Materialen, die zur Instandhaltung notwendig sind, in vollem Umfang erstatten.

- Alle eventuell erzielten Einnahmen werden nicht abgeführt, sondern verbleiben in der Kasse des Netzbiotops/c3d2

- Wir dürfen NICHT in die Wände gehen und die Integrität des Bunkers muss gewahrt bleiben. Das beinhaltet auch die Zwischenwände der Parkplätze/Alkoven.

- 75 Prozent der Bunkerfläche müssen permanent frei gehalten werden. Das beinhaltet nicht die Toiletten und nur 4 der Alkoven. Demnach können wir den Rest permanent nutzen.

- Wir dürfen keine Festanlagen installieren und keine innerhalb von 12 Stunden de-montierbaren Änderungen am Bunker vornehmen.

- Die Lüftungssysteme müssen permanent nutzbar sein, weshalb die Schächte anscheinend (dort streite ich noch) nicht anderweitig genutzt werden dürfen. Zur Erklärung: 4 von 8 Schächten sind nicht genutzt für die Ent-/Belüftung.

- Die Wände mit den Markierungen müssen permanent offen sein und dürfen nicht zugestellt werden.

- Die Schleusen müssen offen bleiben und dürfen nicht zugestellt werden.

Ich glaube das waren alle bisherigen Punkte. Hier laufen keinerlei Kosten in das Budget, da es nur um den Mietgegenstand geht.


== Fußboden ==

Hier ist ja schon bekannt, dass die Planung dahin geht, dass wir Euro-Paletten auf die verwendbaren Flächen (abgesehen von den Seitennischen) packen und mit Holzplatten belegen/befestigen - der Vorteil ist, dass wir direkt Kabelschächte und Abluftschächte im Boden haben und ggf. eine "Heizung" durch die Ausnutzung der warmen Luftströme, außerdem ist der Boden sehr fußkalt, was eine Abgrenzung ohnehin sinnvoll macht.

Da wir ohnehin nicht in die Wände gehen können, bietet es sich in diesem Falle sogar massiv an, entweder an der Decke montierte Kabelschächte zu nutzen oder direkt im Boden die benötigten Kabel mit "Falltüren" zu versehen.

Die Kosten sind minimal, weil wir die Paletten für lau vom Baumarkt bekommen, und belaufen sich nur auf Anfahrtskosten (übernimmt wohl ein befreundeter Spediteur) und die Holzplatten zur Abdeckung, hierfür werden maximal 50 € fällig.

Der etwas teurere Punkt ist das Bespannen der Platten mit Teppichboden - ich halte es persönlich für sinnvoll und notwendig, ein bisschen "Charme" in den Bunker zu bekommen und speziell die Aufenthaltsbereiche auszustatten. Eingangsbereich und die Nischen sind ohnehin davon ausgenommen.

Kostenfaktor für die Bodenbeläge belaufen sich nach derzeitigem Stand auf 370 €. Die Montage erledigen wir selbst, allerdings dürfen wir den Teppich nicht an den Bodenplatten befestigen, da es sonst nicht innerhalb von 12 Stunden zurück gebaut werden könnte. Zumindest falls es einen Notfall innerhalb Dresdens gibt - das halte ich für ausgeschlossen, aber das Amt wohl weniger^^

Ein Muster für die Teppiche lege ich im HQ zusammen mit einer aktuelle Gesamtkostenaufstellung aus.


== Küche ==

Eine der Seitennischen soll zu einer VOLLSTÄNDIGEN! Küche werden, das hat einige Gründe. Insbesondere wird bei größeren Veranstaltungen ein gesonderter Platz für das leibliche Wohl notwendig und auch ein Herd und Ofen werden kaum schaden. Hier habe ich bereits Erkundigungen eingeholt und eine geeignete Einbauküche gefunden, natürlich in dunkler Färbung, damit man den Dreck nicht sieht^^

Der Kostenpunkt liegt bei 510 € plus eine Ofen/Herd-Kombination für 410 €, natürlich elektrisch, aber keine Induktionsfelder.

Hierzu kommen nochmal etwa 100 € für Teller, Besteck etc. und nochmal 200 € für Töpfe etc. Hier habe ich Budget hoch angesetzt, um gleich anständige Einrichtung zu kaufen und insbesondere genug davon.

Über eine eventuelle Geschirrspülanlage wird entschieden, wenn das Budget vollständig steht. QoL^^ Eine kleine ist bereits für 245 € zu haben und genügt unseren Ansprüchen problemlos.


== Dusche ==

An dieser Stelle haben wir das Problem, dass noch keine Besichtigung möglich war. Allerdings gibt es einige Auswahlmöglichkeiten, die ich im HQ auslegen werde, damit jeder sich anschauen kann, worauf es hinauslaufen soll. Die Dusche soll mit Glas "verkleidet" sein (Lotuseffekt inklusive, um die Reinigung zu erleichtern) und im LINKEN Badezimmer stationiert werden.

Ich halte eine Dusche für durchaus sinnvoll, auch weil das Bunkerprojekt durchgehend laufen soll und bis dato ohnehin einige (omg was für Nerds ^^) öfter im HQ nächtigen, leider mich inklusive. Auch ist es bei größeren und längeren Veranstaltungen vorteilhaft, eine Möglichkeit zu haben, um vor Ort der Hygiene zu frönen^^ - nichts ist schlimmer, als mitten drin nach Hause zu müssen, auch weil die Wege für Einige nicht unerheblich sind.

Weitere Infos werden folgen, sobald ich einen Kostenvoranschlag zur Hand habe.


== Chillarea ==

Die Chillarea soll ein rein gesonderter Bereich im Bunker werden und deshalb auch definitiv abgegrenzt werden - ob wir das mit schweren vorhängen oder Gips-/Holzwänden realisieren, ist noch ungeklärt. Das hängt schlicht vom Preis und von der Geräuschkulisse ab und wird wohl erst in den ersten Wochen im Bunker bzw. in der Trocknungsphase endgültig geklärt werden.

Allerdings steht die Inneneinrichtung bereits fest. So wird das größte Ikeabett auf niedriger Höhe bis auf 5 cm pro Seite in die Räume passen und soll mit Kissen, Decken und kleinen Seitentischen ausgestattet werden. Der Preisfaktor beläuft sich auf insgesamt 390 € und beinhaltet die komplette Einrichtung inklusive 2 dünne Matratzen, die übereinander gelegt werden (weicher = besser ^^).


== Löt-Zone/Werkstatt ==

Hier bewege ich mich auf dünnem Eis - der Bereich wird in jedem Falle gesondert sein, um Allen, die Projekte haben, den Zugang zu ermöglichen. Weitere Infos sind mangels Absprache noch nicht vorhanden, ich schließ mich aber mit Alex kurz.


== Arbeitsbereich ==

Der Arbeitsbereich soll ähnlich wie die Chillarea abgegrenzt werden und NUR für Arbeitszwecke dienen, bzw. um einen Ruhepuls innerhalb der Räume zu haben. hier sind 1. richtige Tische als Arbeitsoberfläsche und Bürostühle (mit Rollen) geplant, ebenso natürlich eigene Anschlüsse ans Netz.

Die Kosten hierfür werden sich auf etwa 370 € belaufen. Das beinhaltet derzeit 4 Sessel und einen großen und hohen Glastisch - für Vorschläge, was diesen Bereich betrifft, bin ich nach wie vor SEHR dankbar.


== Internet ==

Wir werden im Bunker eine E3-Leitung (34 Mbit/s) bekommen können. Der derzeit (mangels anderer Angebote) beste Anbieter ist T-offline. Die Anschlussgebühren belaufen sich auf 720 €, monatliche Gebühren sind wie gehabt.

Die Gräben müssen wir selbst anlegen, die Frage nach der Tiefe ist dabei erstmal nachrangig und muss mit dem Amt besprochen werden, weil wir ggf. eine maximale Tiefe bekommen.


== Kostenfrage ==

Derzeit habe ich ein Konto eröffnet, auf dem ein Großteil des Budgets bereits vorhanden ist (8500 € derzeit) und zu dem ausgewählte Personen im Kreise des Netzbiotops/c3d2 Zugang erhalten, um für den Fall, dass ich verhindert sein sollte, Zahlungen tätigen zu können.
=Kosten=
==Baukosten==
Gräben buddeln, Löcher bohren, Elektrik instand setzen, bis zur Errichtung von Zwischenwänden und dem Fussboden.

Wenn die Stadt kosten übernimmt, dies dazu schreiben. Aber wir sollten zunächst davon ausgehen dass die Stadt bei der aktuellen Finanzlage NICHTS übernimmt.

{| border=1
!Baumaßnahme !! Kosten !! Beteiligung Stadt !! Bemerkung
|-
|Gräben Internet || ? || ? || als Beispiel
|-
|Rigips || ? || ? ||
|-
|Kabel || ? || ? ||
|-
!Gesamtkosten !! !! !! zwar geschätzt, bitte aktuell halten
|}

==Einrichtungskosten==
Unsere Einrichtungsgegenstände. Von Fussbodenbelag über Regale, Tische, Stühle, Betten.

{| border=1
!Einrichtungsgegenstand !! Kosten !! Bemerkung
|-
|Internet || 720 € || einm. Anschluss T1 Leitung T-Kom
|-
|Europaletten || 50 € || Kostenlos vom Baumarkt, nur Transport
|-
|Teppichboden || 370 € ||
|-
|Küche || 510 € || [http://img412.imageshack.us/img412/5271/cimg0079n.jpg Bild Küche]
|-
|Herd/Ofen || 410 € ||
|-
|Geschirrspüler || ab 245 € ||
|-
|Teller || 100 € ||
|-
|Töpfe || 200 € ||
|-
|Dusche || ?? || Warten auf Kostenvoranschlag
|-
|Betten || 390 € || Für 1 Doppelbett + Matratzen
|-
|Arbeitsbereich || 370 € || 4 Sessel, Glastisch
|-
!Gesamtkosten !! 3.365 € !! zwar geschätzt, bitte aktuell halten
|}

==Laufende Kosten==
Alles was monatlich anfällt - v.a. Strom, Wasser, Müll, Maintenance wie Rasen mähen. Zumindest ne grobe Kalkulation kann hier genutzt werden. Vllt. auf Sublab und HQ Erfahrung zurückgreifen

{| border=1
!Posten !! Kosten geschätzt !! Kosten momentan (HQ 1.0) !! Bemerkung
|-
|Miete || - || >120 € || Serveraum
|-
| || - || 175 € || Rest
|-
| || 1 € || ~300 € || Gesamtmietkosten
|-
|Strom || ?? || 30 € || Strompauschale HQ 1.0
|-
|Wasser || ?? || - ||
|-
|Heizung || ?? || - ||
|-
|Internetleitung || ?? || 140 € || Zahl die ich in dem Zusammenhang von Alex mal gehört habe (koeart)
|-
|Traffic || ?? || 350 - 450 € || Neue Leitung, andere Kosten?
|-
|IP's || s. HQ 1.0 || 40 € || wird sich ja nichts ändern
|-
!Gesamtkosten !! > 141 € !! >910 € !! zwar geschätzt, bitte aktuell halten
|}

=Nutzung=
Für was wollen wir den Bunker nutzen? Welche Veranstaltungen schweben euch vor? Danach richtet sich z.B. die Raumaufteilung und anderes

{| border=1
! Aktivität !! Größe/Personen !! Häufigkeit/Monat (wenn bekannt) !! Idee von../Ansprechpartner !! Bemerkungen
|-
|Podcaststudio || 5 || mehrmals || wahrscheinlich Pentamedia-Team, koeart || Wenn mans denn Schalldicht bekommt und Coloradio tot ist...
|-
|Geekends || 10 || 1-2 || der jeweilige Organisator || Juhuu, endlich!
|-
|Werkstatt || 3-5 || mehrmals || alle || ja ne is klar, oder?
|-
|Chill/Coding-Area || 12 || mehrmals || alle || mindestens so viele, wie ins HQ passen ;-)
|-
| || || || ||
|}

=vorläufige TOPs=
[http://ftp.c3d2.de/misc/bunker-discuss.txt bunker-discuss.txt]
=Protokoll Bunker Treffen=

* 15.03.2010
* Anwesend: Astro, koeart, carwe, thammi, blotter, blitz, marcus, dodo, k-ot, r0oland, unicum, farao, blub, core und andere die nicht mit am Dokument schreiben (15+ Personen)

== Zuständigkeiten, Finanzierung, Pläne & Wartung ==
* Bautrockner sind low-cost (evtl. no-cost) via SmOOkiE (unicum) verfügbar
* Stadt zahlt Materialaufwand den wir beim Reinigen haben
* Miete 1€/Jahr
* Allg. gilt, Kosten die von Stadt übernommen werden, stehen im Mietvertag
* Mietdauer bis jetzt mind. 2 Jahre
* Kündigungsfrist: 3 Monate
* Was wird überhaupt alles im Mietvertrag geklärt??
* Zivilschutz in diesem Bunker?
* Zivilschutz weiß nichts vom Bunker
* Ansprechpartner: Bundesamt für Liegenschaften unser 
* nur alte Pläne (Grundriss) von 1979 vorhanden
* Besichtigungstermin: Mitte April (16./17./18.)
* Instandhaltung & Wartung muss von uns durchgeführt werden
* TÜV für gemeinnützige Vereine gratis
* Instandhaltungskosten für Material können wir der Stadt in Rechnung stellen
* Materialkosten für Trocknung können teilweise in Rechnung gestellt werden
* beim Amt für alles Anträge stellen (Sammelantrag)
** Bürokratiemühlenarbeitszeit bedenken! kann wahrscheinlich dauern bis 
** son Antrag durch ist...
* keine Pacht für gemeinnützigen Verein
* Mietverhältnis muss zum 15.6. oder 1.7. beginnen, bedeutet jedoch nicht,
* dass wir zu diesem Zeitpunkt wirklich einziehen müssen
* 75% der Hauptfläche und vier der acht Alkoven (ohne Schleusen & Bäder) 
* dürfen nicht permanent zugestellt sein 
** müssen im Notfall innerhalb 24h geräumt werden
* Lüftungssysteme: vier von achte Schächten müssen permanent offen bleiben
** Kosten für Ersetzung der Luftfilter übernimmt Stadt

== Beleuchtung ==
* Originallampen bleiben drin (werden aber nicht benutzt), 
* indirekte Beleuchtung über Decke (Rundgewölbe)

== Luftschleusen ==
* dauerhaft offenlassen
* Schleuse nur von Innen immer zu öffnen, von Außen wird Schlüssel benötigt
* auch nie Absperren wenn Polizei draußen steht
* Außentüren aber schon schließbar

== Fußboden und Decke ==
* 7.5m Platz nach oben bis zur Kuppel (3.5 meter bis zur kuppel 
** (höhe des gekrümmten bereiches))
* Europaletten aus Holz, Holzplatten drüber
* Kabel easy durch Paletten, Falltüren
* Boden ist superkalt (dicker Stahlbeton)
* Kein Feuchtigkeitsproblem, heiße Serverabluft wird runtergeleitet
** das ergibt auch gleich ne geile Fußbodenheizung, *WIN WIN*
* Planung als Segmente, Paletten jederzeit wieder wegräumbar
* oberste Bodenschicht soll Holz (Hartholz oder Spanplatten), 
**(vermutlich Pressspanplatten, billig und einfach zu verbauen)
* auf jeden Fall mit Schutzlack 
* Teppichboden in der Couchzone
* Heizstrahler im Winter oder Methangasbetriebene Brenner

== Finanzen ==
* Problem: jetziger Verein: wackelige finanzielle Situation
* Komplettes Kapital zum Ausbau wird von Accuso & Alex getragen, als Spende
* Bedenken von Farao bezüglich Finanzierung durch 2 Privatpersonen
* Möglichkeit: als Zinnslosen Darlehen das CCC 2112 zurückzahlen muss,
** ich persönlich sehe nicht das Problem warum wir es nicht als Spende annehmen
** da damit ja keinerlei Verpflichtungen vom Verein an die Spender bestehen
** wichtig das es VOR dem eigentlichen Projektstart geklärt ist und bevor 
** von irgendwem irgendwelches Geld bezahlt wird
** vermutlich sollte keine Privatperson irgendwas bezahlen oder?
* ??? Trennung der Finanzen zwischen Bunker und HQ ???
* evtl. Vertragliche Regelung
* von den ca. 8500€ wird Einrichtung bezahlt, der Rest (Beamer, HiFi, 
** Mediaserver, Ausrüstung für Werkstätte, (Technik im allgemeinen)...) wird 
** mit späterer, von 8500er losgelöster Spende, bezahlt

== Küche ==
* Ist verfügbar, kann zum 1.5. eingebaut werden
* Sehr groß, mit Essecke, Geschirrspüler, günstig für GeekEnd
* gern gesehen wäre Graskaffee, Brownies, Cookies, "Totaler Durchblickstrudel"
* wenn dort der Schimmel so gut wächst ---> wie wäre es mit Magic Mushrooms?
* Accuso will eine, koeart sieht nicht viel Sinn 
* http://img412.imageshack.us/img412/5271/cimg0079n.jpg

== Dusche ==
* Damen-/Herrenbad
** müssen nicht getrennt werden, können eins voll stellen => Duschen
** Zählen nicht zur Hauptfläche (75% ...)
* werden reingebaut
* Dusche sehr schön für Dort-Penner :-)
* Verhältnismäßigkeit gegeben wenn günstig verfügbar
* Argumente dagegen gibts nicht oder?
* Kein Einbau ohne Besichtigung

== Zen-Garten ==
* Gute Idee
* Steine, Bonsaibäume, Hanfpflanzen
** entsprechende Beleuchtung
** c3d2-Chilianbau ;)
* Wollen wir?

== Chillarea ==
* gesonderter Bereich (keine Sitzecke)
* Keine Sessel
* Lesen, Musikhören, Rumliegen, Schlafplatz
* Große Liegeflächen aus einem Ikeabett (geplant)

== Arbeitsbereich ==
* mit vernünftigen Tischen
* Coworking
* 4-8 Sessel geplant
* Ruhe und Ordnung so das ordentliches Arbeit möglich ist

== Kabel & Internet ==
* Bohren für Kabel für Internet, mit PVC dick ummanteln
* können 2 eigene Leitungen bekommen
* Kabel kann nur am Gehwegrand gelegt werden, da der Rest um den den Bunker 
* drumherum für Katastrophenfall frei bleiben muss 
* wir dürfen prinzipiell nur 20 cm tief graben (weil Erde im Ernstfall zum Schutz auf den Bunker geschoben werden soll)
* wird kontrolliert beim Einbau, deshalb Kabel ummanteln, 
* 1.5m tief, anständig abisolieren
* Entweder T-Online: oder QSC:
** T-Online: E3 - 720 € Anschlußgebühren) mit 50 IPs
*** monatlich 370 €
*** Gräben wäre selbst zu schaufeln, aber Bunkeranschluss kein Problem
** QSC: 2 E2 Leitungen - 205 € Anschluss mit ~30 IPs
*** Gräben würde QSC schaufeln lassen, aber Bunkeranschluss müssen wir machen
*** ???monatlich oder Anschlussgebühren???
* hoffen auf ipv6 <-- wird enttäuscht :-( also kostenlosen Tunnel
* VDSL keine Alternative zu Standleitung
* bei Ausfall Regressansprüche, damit viel zuverlässiger
* Kein Ethernetanschluß hier in Dresden, vor allem nicht im Wohngebiet
* 2. Idee: eine E1/T3 Standleitung für unsere Dienste & 50Mbit VDSL etc. für normal Gebrauch

== Strom ==
* Internet könnte mit den Mitteln der alten HQ-Miete bezahlt werden
* Mehr Aufwand für Beleuchtung (Indirekte Strahler auf die Decke) --> höhere Betriebskosten
* 5 Starkstromleitungen
* freie Anbieterwahl (envia am besten?)
* 60-70€/m? <--- ?unwahrscheinlich?
* Serverraum: eigene Leitung, von Serverbetreibern bezahlt

== Heizung ==
* Passive Lüftung von Servern
* Heizstrahler/-lüfter
* Überdruck durch Aufwärmung

== Trocknung ==
* Professionelle Firma -> Teuer
* selber Trocknen -> Brenner
* Salztrocknung ->1T 50€, legts aus, kehrt's zusammen, legt's raus

== Schimmel ==
* Oi oder Punk spielen (danach Techno)
* Verkeimungen "nur" Oberflächlich -> Chlorix
* Genehmigung evtl. nötig
* Gebäudereinigungsfirma
* Gasbrenner + Abfackeln <- aufpassen, dass die Flamme nicht ausgeht ;)
* erst entkeimen, dann entlüften

== regelmäßige Arbeiten ==
* Accuso bezahlt Gärtner
* Sanitäre Anlagen & Küche müssen selbst gereinigt werden
* Küchendienst nicht vorteilhaft, denn wenn man seinen eigenen Mist nicht weg machen muss, dann tendiert man zu größeren Sauereien
* Aufgaben wie im HQ auch

== Werkstatt ==
* Aus dem HQ übernommen, keine Server
* größer bzw. größere Arbeitsflächen
* Ausstattung? (Werkzeuge, Maschinen)
** Feinwerkwerkstatt
** Grobwerkstatt (Schmutzentwicklung beachten)
*** gesonderte Absaugung/Staubfilter?
* Biohacking-Area / Pflanzen hacken (Chilli)
* Geekends
* Filmabende
** Beamer & Leinwand?
* Hackerspace-Treffen (aus anderen Städten)
** young/and/lost Hacker-Hostel falls mal wer nen paar Tage in DD is (z.B. DS)
** trust-basis
* (live-)Podcasting
* abhörsicherer Raum / inkl TEMPEST? (hochfrequente El-Mag-Strahlung, von Monitoren z.B.)

== Veranstaltungen ==
* bis 200 Leute Anmeldungsfrei
* wieviele passen sinnvoll in den Bunker? (Aufenthalt/Übernachtung)

== Mitnutzer ==
* wunderbar
** unicum hat konkrete Vorstellungen 
** Bar Projekt -> will selbst Dinge initiieren
** wollen Vernissage etc veranstalten
** Dauer: ca 1 Wochenende (evtl. jährlich - max halbjährlich - wiederkehrend)
* Interesse von Leuten die dort Kinoabende machen wollen
* CCC Hamburg: möchte dort kleinere Schulungen etc machen
* Partnerschaft mit anderen Labs -> Erfahrungsaustausch etc
* evtl LUG-DD? Linux-Tag?

== Organisation ==
* offene Strukturen teilweise durchbrechen
** Teams die gegen geringes Entgelt/frei Mate etc kriegen, im Gegenzug Pflege/Mateausschank
** Teams bilden, Serververwaltung, Barteam, etc pp -> Schwarmintelligenz nutzen

== Sonstiges ==
* 24h Räumung -> evtl anderen Bunker nehmen (wo ist der?)
** Liliengasse http://www.sachsenschiene.net/bunker/bun/bun_74.htm
*** möglicherweise http://maps.google.com/maps?q=51.047+13.72804
*** (Rampe vorhanden, aber ich seh keine Lüftungsschächte etc. wie bei Hans-Dankner-Str.)
** Seidnitzer Str. http://www.sachsenschiene.net/bunker/bun/bun_75.htm
*** nicht gefunden auf Google Maps (dabei ist die Rampe eig. markant)
*** ist auch ein verdammt großes Gebiet
** einer von beiden ist ein SBW-600
*** http://www.sachsenschiene.net/bunker/sys/map.htm (wie belastbar ist die Quelle, ist auch nicht konsistent (sagt 1x SBW-600 & 2x SBW-300 in DD, weiß aber nicht, wo was ist))
* neuer Verein?

= UPDATE 09.04.2010 =
<pre>From: Accuso <*@*>
To: "c3d2@mail.skyhub.de" <c3d2@mail.skyhub.de>
Subject: [c3d2] Hq 4.0
Date: Fri, 09 Apr 2010 21:54:03 +0200

hi @ all

hiermit verteil ich mal neue infos an euch zwecks dem Hq 4.0 aka. 
Bunkerprojekt

derzeit ist der stand der dinge besser überschaubar geworden ...

1. das amt für zivilschutz ist NICHT in dem bunker drin ... zumindest 
nicht vertraglich abgesichert
2. demnach hat es nix zu melden und muss ggf. seine Fahrzeuge/Gedöhns 
dort rausholen
3. das amt für katastrophenschutz hat eine zuständigkeitsbeschwerde am 
arsch weshalb sie ebenso die schnauze zu halten haben

demnach bleiben nur 2 ämter übrigen ... liegenschaften und das amt für 
innere angelegenheiten

soll heißen uns stehen nur noch 2 dinge im weg ... eine besichtigung und 
ein folgender mietvertrag ^^

deshalb wird es wohl in der woche vom 19.-23. eine besichtigung geben 
... genaue daten habe ich noch nicht aber sie werden folgen


gez. Axxusosan</pre>

= UPDATE 01.05.2010 =
<pre>From: Accuso <*@*>
To: "c3d2@mail.skyhub.de" <c3d2@mail.skyhub.de>, 422342@googlemail.com
Subject: [c3d2] hq 4.0 Bunker
Date: Sat, 01 May 2010 19:15:49 +0200


an guaden meine lieben

leider ist der grund für die folgende mail diesmal erneut ein sehr 
unerfreulicher... auch weil die wirren um den bunker kein ende nehmen... 
aber @ topic

Bunker Informationssammlung:

Wir wissen derzeit mit recht großer Gewissheit, dass der Bunker nicht 
von anderen Ämtern in Benutzung ist. So existiert laut Auskunft des Amts 
für Liegenschaften kein Miet- oder Pachtvertrag... weder mit Ämtern der 
Stadt oder des Landes noch mit Privatpersonen. Weiterhin steht fest das 
das Amt für Katastrophenschutz ebenso nichts mit dem Bunker zu tun hat 
außer womöglich noch im Bunker befindlichen Fahrzeugen... die natürlich 
geräumt werden müssen

Darüberhinaus ist aber bis dato jeder termin aufgrund von falschen 
absprachen oder kompetenzproblemen innerhalb des amtes gescheitert ... 
weshalb es bis dato auch recht still wurde um das projekt bunker...

aufgrund dieser internen probleme werd ich versuchen neue wege zu gehen 
in betreff hq 4.0 und schau mich nach anderen lohnenden alternativen um 
... speziell in der neustadt

der bunker ist NICHT gestorben ... dort wird es natürlich was planung 
und ämter betrifft weiter gehen ... nur wird es langsam aber sicher 
notwendig das wir uns nach anderen alternativen umschaun... wobei 
natürlich auch weitere räume innerhalb der lignerallee gemeint sind :-)

in diesem sinne ... carry on ... für vorschläge eurerseits wäre ich sehr 
dankbar... alles weitere zu meinem neuen planungsstand bekommt ihr 
sobald wie möglich

gez. Accuso</pre>

[[Kategorie:HQ]]
