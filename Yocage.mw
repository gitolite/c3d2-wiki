{| class="wikitable"
|-
! German / deutsch
! English / englisch
|-
| 
=== Projekt ===

==== Ausgangssituation des Projektes ====

[[PC-BSD]] hatte [[Warden]]. [[Warden]] ist (war) ein Werkzeug zur Verwaltung von [[Jail]]s. Eine Besonderheit von [[Warden]] ist (war), dass die Verwaltung auch mit einer grafischen Oberfläche ([[Qt]]) möglich ist (war).

[[Warden]] wurde durch [[iocage]] ersetzt. [[iocage]] bietet jedoch keine grafische Oberfläche. Praktisch wurde die grafischen Oberfläche ersatzlos gestrichen. Ein Ersatz für die grafischen Oberfläche ist höchstens angekündigt.

[[AppCafe]], das Werkzeug zur Verwaltung von Software von [[PC-BSD]], wurde von einer grafischen Oberfläche ([[Qt]]) auf eine grafische Oberfläche als Browser umgestellt. Zweck ist es, dass auch die Verwaltung der Software ([[AppCafe]]) aus der Ferne mit einem Web-Browser möglich ist.

==== unser Idee ====

Ein Ersatz der grafischen Oberfläche soll, wie es sie bei [[Warden]] gab, soll geschaffen werden.

Anstatt eine grafischen Oberfläche mit [[Qt]], wie es bei [[Warden]] der Fall war, zu erstellen, soll in Anlehnung an das Konzept zur Verwaltung der Software ([[AppCafe]]) ein Browser verwendet werden. Die Verwaltung von [[Jail]]s soll somit auch aus der Ferne mit einem Web-Browser möglich sein.

==== unser Ansatz ====

Wir bauen für [[iocage]] (Verwaltung von [[Jail]]s für die Kommandozeile) eine Web-Anwendung.

Eigentlich müsste die Realisierung unabhängig der Komponenten von [[PC-BSD]] gegenüber [[FreeBSD]]

|
=== project ===
|-
| 
==== Komponenten ====

===== [[FreeBSD]] =====

Wir benutzen [[PC-BSD]] (bzw. [[FreeBSD]]), da [[Jail]]s "schön", bequem und verhältnismäßig sicher sind.

Im Übrigen kann auch an eine Portierung für [[DragonFly BSD]] oder [[OpenIndiana]] gedacht werden.

===== [[iocage]] =====

Wir benutzen [[iocage]], da es sehr einfach und bequem scheint und ohne große Konfigurationsdateien auskommt.

Nennenswerte Alternative dazu wäre vielleicht nur [[ezjail]].

===== [[Yesod]] =====

In Anlehnung an den Erfolg vom [[Matemat]] wird [[Yesod]] eingesetzt. Yeah!

===== Ressourcen =====

* Projektmanagement: [https://redmine.c3d2.de/projects/yocage redmine.c3d2.de/projects/yocage]
* Repository: tbd
* Dokumentation: tbd
| 
|-
|

==== Diskurse zum Projekt ====
|

|-
| colspan="2" |
====== Transcript KenMoore/KrisMoore 2015-09 ======
<tt>
; tboston: is it okay if we try to make a web interface for iocage? thats our plan for now. or is it not needed since you want to keep that in AppCafé?
; kmoore:  hey, if you guys want to make one, go for it, I can't stop ya :) The plan is to put one into AppCafe also, but having alternatives is always good
; tboston: but if you don't use it in the end its somehow senseless
; kmoore: '''true. The plan is to have one in AppCafe which is also in FreeNAS / TrueOS, etc'''
; tboston: kenmoore: what was that admin-tool you talked about?
; kenmoore: tboston: admin-tool?
; tboston: yeah you said something about an admin gui for pcbsd would be nice
; kenmoore: '''Oh, I was talking about a unified administration framework/system (such as a library) for the system''' so yeah, I guess this would start counting as that. '''basically need something on the low-level to know about all the various FreeBSD files/settings/etc, so top level GUI's/tools only need to run CMDs through a single utility makes it a lot easier to maintain system tools and UI's down the road'''
; tboston: okay, I'll keep that in mind
; tboston: can you be more specific on that low-level tool? Like what would it need and what is the OS/Admin getting out of it?
; kenmoore: Well, I was thinking about 2-components:
; kenmoore: '''1) A single libary (something like libSysAdmin), with all the various low-level system interface routines (in C/C++/QtCore - any of them will work) This is the "unifier" for accessing all the various FreeBSD subsystems (networking, user management, etc..) because every subsystem seems to have their own files/formats/etc'''
; kenmoore: '''2) The user interface (web-based, or local) which just needs to load that single library to read/change things on the system as necessary. This webclient tool that we are working on could easily become a #2, but #1 does not exist yet. #1 will probably need a couple different libs actually - a "user" level one for providing read-access for supported things, and a "root" level one for modifications so maybe a libsysadmin-[user/root] setup'''
; tboston: okay, I think I got it
; kenmoore: that way the #2 interfaces can only load the libs they are able to actually use. this would be nice because unlike  systemd and other Linux-y tools it does not ​replace​ the FreeBSD admin tools - it simple provides a simple interface to them. all the old methods will still work just fine - the libs just provide a single point of entry to be maintained/updates as FreeBSd changes
; tboston: do you know of a linux equivalent?
; kenmoore: not really - I am not too familiar with Linux utilities aside from some basic generalities
; tboston: k
; kenmoore: I think this type of setup would be unique to FreeBSD - since I hear the Linux people complain all the time about a new system meaning how they change setting X is now completely different. this would basically provide a standard API for system calls without changing the underlying OS/infrastructure
; tboston: understood
; kenmoore: '''I started working on one for a little bit (networking first), you can see what I have in pcbsd/src-lib/pc-utils specifically the NetDevice.cpp file. that does a lot of the network probing on a very low C interface level. oh, and the pcbsd-network.[cpp/h] files'''
; tboston: let me check that
; kenmoore: '''note that I was creating the library with C++/Qt though - so if you want to do a pure C library implementation you might need to change some things'''
; kenmoore: '''mainly convert the QString's to a C string and things like that'''
; tboston: gotta look into that when I get home
; tboston: but guess would be fine as a base
; kenmoore: I had to put that effort on hold since I just got too busy with other stuff
; tboston: if you dont mind
; kenmoore: I don't mind
; tboston: anyway, our Haskell guy is unemployed then...
; kenmoore: :simple_smile: '''This is easily a full/huge project, so I started tackling it by breaking it up into all the various subsystems. I recommend a similar approach so that you can simple add/enable new subsystems instead of breaking existing stuff. keep it all compartmentalized'''
; tboston: is pbsd/pc-utils all part of it?
; kenmoore: pcbsd/src-lib/pc-utils was all of it
; tboston: ahh yeah thats what I meant
; kenmoore: '''I created the shell for the GUI interactions (src-lib/pc-gui) but there is really nothing there. the lib needs to be more complete first so the API is set before we can start writing GUI interactions with it. note that for some subsystems (like networking) FreeBSD is designed around OS-utility use - not direct library access/control (such as using "ifconfig"), so for making changes you might need to actually run that utility. otherwise you might end up duplicating a lot of the code within ifconfig, and break the future-proofing of the API (basically maintaining a fork of an OS tool) just FYI :simple_smile: usually just reading the system info can all be done through libs though'''
; tboston: well, sounds huge like you just said :simple_smile: but hey, we're 4 ppl wanting to get involved
; kenmoore: give each person a subsystem and get started! :stuck_out_tongue:
; tboston: haha yeah and we need to integrate Haskell somewhere :stuck_out_tongue:
; kenmoore: yeah, that too. I recommend making a general class of static functions for use across all subsystems. such as reading a sysctl
; tboston: yep
; kenmoore: anything to make it easy for others to read/update later is good... :simple_smile:
; tboston: Haskell can talk json/websockets, we might test that out some time
; kenmoore: yeah, JSON/websockets are fairly universal. because it is basically a standardized ​format​ rather than a standardized ​utility​. so any tool/utility can support the format as necessary. That is actually my problem with a lot of the linux stuff - they have recently been pushing standards in the form of particular utilities/tools instead of a standardized interface format. DBUS/systemd are good examples. tools are restrictive (OS-support, library requirements, etc). while formats are universal
</tt>
|-
| colspan="2" |
====== Transcript KrisMoore 2015-10 ======
<tt>
; kmoore: around? Hey, don't spread this in the chat channel yet, but do you still want to hack on AppCafe UI?
; kmoore: Looks like we may be going forward with our existing one, which means if your group wants to do some work on jails / vm / other, that could end up going into PC-BSD / TrueOS / FreeNAS
; tboston: hey
; tboston: sounds good
; tboston: now, the jail stuff won't be kept in app café then or what just changed?
; kmoore: no, it will be, the plan is to turn AppCafe into a one-stop app for all things App related
; kmoore: like System Packages - App Jails - Jail Management - VM Management
; tboston: okay good
; tboston: and we should talk about that directly with you? or who's done that until now?
; kmoore: talk to me about it for now, kenmoore is doing some backend stuff, and joshms has helped with css / theme
; tboston: we actually started with pcutil already
; kmoore: nice!
; kmoore: :stuck_out_tongue:
; tboston: so now we have two projects
; tboston: I'll go see my colleagues later and see what we can do
; tboston: btw seen my pull request?
; tboston: tested that on a freebsd vm already
; kmoore: yep, leme get that put in
; kmoore: done!
; tboston: you'd better check again
; kmoore: oh? it looked fine, did I miss a comment?
; tboston: no no
; tboston: just because I'm still not always sure if I did everything correct
; tboston: although I tested it
; kmoore: oh, it looked good to me, i looked at the diff :simple_smile:
; tboston: k
; tboston: ttyl, still at wok
; tboston: work*
; kmoore: ok, ttyl!
; tboston: so
; tboston: off work
; tboston: I'm still not sure what you actually need with app café
; kmoore: nothing specific at the moment. Its just that you expressed interest in hacking on jail management UI
; tboston: yeah sure
; kmoore: since we aren't having a 3rd party do it now, we are going with the existing UI, and I'm hacking on jail stuff at the moment
; kmoore: so if you guys want to send patches, improvements, etc. those will be welcome
; tboston: we'd like to make a project out of it
; tboston:  atm you have that 'plugin' thing in app café
; tboston: I guess this will be thrown away and you'd want real jail management + vm management
; kmoore: well, yes and no
; kmoore: our current plan is to have the AppCafe do this:
; kmoore: System Packages - Current way of installing PKG stuff on PC-BSD / TrueOS server
; kmoore: App Containers - Pre-Built jail images, we can download / depoy with single click
; kmoore: Jail Manager - Full iocage jail UI, for power users who roll / run their own jails
; kmoore: VM Containers - Pre-Built VM images which can be downloaded / run with single click
; kmoore: BHyve Manager - Full bhyve UI, power users, etc
; kmoore: I.E. we want to have an "easy" option for jails/vms, and then the full-blown advanced mode, for l33t users :stuck_out_tongue:
; kmoore: Often a home user just wants to deploy plex, but they have no idea about jails/vm, so container is way to go there
; tboston: okay
; kmoore: no mess, no fuss
; tboston: and you NEED that in AppCafé?
; tboston: or could that iocage/bhyve stuff be seperate
; tboston: in a webgui
; kmoore: Well, the AppCafe is a webgui, so thats part of it yes
; kmoore: ideally we wanted it all there, so its a single web-driven location you can use to control all apps/pkgs/jails/vms on a system
; kmoore: but it could be separated into distinct components as well if necessary
; tboston: and then like an 'advanced' tab for power users
; kmoore: more or less, yes
</tt>
|-
|}
