[[Kategorie:Wissen]]
[http://smarden.org/runit/ runit] ist ein freier [http://cr.yp.to/daemontools.html daemontools]-Ersatz,
der auch sysvinit ersetzen kann. Runit überwacht System-Dienste und startet sie neu, wenn sie beendet
werden. Desweiteren bietet es ein Logging-Framework, so dass bei den Diensten Logging nicht extra konfiguriert
werden muss sondern, sondern diese einfach nach Standard-Fehler oder Standard-Ausgabe loggen können.

Dienste leben unter runit/daemontools im Verseichnis /service (bzw. /var/service, es bietet sich aber bei runit an,
einen Link von /service nach /var/service zu setzen). Dieses Howto sieht vor, dass jeder OpenVPN-Tunnel seinen eigenen
Dienst hat, der in /service/openvpn-$peer läuft, wobei peer ein beliebiger für den Tunnel gewählter Name ist.
Die Konfiguration für jeden Dienst befindet sich im Verzeichnis /etc/openvpn/$peer. OpenVPN wechseln vor dem Auswerten irgendwelcher Skripte oder Konfigurationsdateien in dieses Verzeichnis, so dass sämtliche Pfadangaben relativ zu diesem
Verzeichnis sein können. Das Setup in diesem Howto ist so ausgelegt, dass ein neuer Tunnel in zwei einfachen Schritten
angelegt werden kann:

# /etc/openvpn/$peer füllen
# Folgende Befehle ausführen:
 $ cp -r /etc/runit/runsvdir/all/openvpn /etc/runit/runsvdir/all/openvpn-$peer
 $ ln -s /etc/runit/runsvdir/all/openvpn-$peer /service

Danach kann man mit
 $ tail -F /service/openvpn-$peer/log/main/current
beobachten, ob der Tunnel korrekt aufgebaut wurde.

Nun zur eigentlichen Vorbereitung: Es müssen zwei Skripte in /etc/openvpn angelegt werden, die später das Starten des
OpenVPN-Tunnels und des zugehörigen Logging-Dienstes steuern.

'''/etc/openvpn/run'''

 #!/bin/sh
 exec 2>&1
 
 # get peer name from service name
 peer=`basename $PWD |  sed -e 's/openvpn-//'`
 
 exec /usr/sbin/openvpn --cd "/etc/openvpn/$peer" \
     --log-append /dev/stderr \
     --suppress-timestamps 0 \
     --config  "/etc/openvpn/$peer/config"

Das zweite Skript ist etwas komplizierter. Es sorgt dafür, dass die Logs im Verzeichnis /var/log/openvpn/$peer
landen. Existiert dieses Verzeichnis nicht, wird es angelegt. Aus Sicherheitsgründen läuft der Logging-Dienst
als unpriviligierter Nutzer. Es empfiehlt sich, hierfür einen eigenen Nutzer anzulegen und nicht nobody zu nehmen,
da diesem dann die Log-Dateien gehören.

'''/etc/openvpn/logrun'''

 #!/bin/sh 
 exec 2>&1
 
 # id to log as
 loguser=openvpnlog
 # where to log to
 logbasedir=/var/log/openvpn
 
 # get peer name from service name
 cd ..
 peer=`basename $PWD |  sed -e 's/openvpn-//'`
 cd -
 
 logdir="$logbasedir/$peer"
 ln -sfn $logdir main
 
 if [ ! -d $logdir ] ; then
        mkdir $logdir
        chown $loguser:`id -n -g $loguser` $logdir
 fi
 
 exec /usr/bin/chpst -u$loguser /usr/bin/svlogd -t main

Unter den daemontools muss die letzte Zeile
 exec /usr/bin/setuidgid $loguser /usr/bin/multilog t main
heißen.

Jetzt muss nur noch das Service-Verzeichnis /etc/runit/runsvdir/all/openvpn angelegt werden:
 $ mkdir -p /etc/runit/runsvdir/all/openvpn/log
 $ ln -s /etc/openvpn/run /etc/runit/runsvdir/all/openvpn
 $ ln -s /etc/openvpn/logrun /etc/runit/runsvdir/all/openvpn/log/run

Fertig! Happy tunneling!
