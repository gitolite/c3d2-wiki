== Entwicklungsstand ==

Derzeit ist der manuelle Schalter in der Lage den Status u.a. über die Website zur Verfügung zu stellen.

''Vorgesehen ist:''
* [https://redmine.c3d2.de/issues/5 Keymatic]
* [https://redmine.c3d2.de/issues/106 opendoor via SSH]
* [https://redmine.c3d2.de/issues/107 Status am Schild] neben der beschränkten Wanddurchgangsöffnung
* [[Klingel]] neben der Eingangstür um auch bei Geräuschkulisse erhört zu werden

== Schalter ==

Schalter ist der große Kasten mit dem 3-Positionen-Schalter in der Mitte direkt an der Eingangstür.

In ihm befindet sich ein Raspberry&nbsp;Pi (Pi) sowie eine Platine, die den Türsummer antreibt.<ref>[https://media.c3d2.de/u/astro/m/schalterinnereien/ Bild der Innereien vom] [[{{PAGENAME}}|Schalter]] by [[Astro]]</ref>

Mit an der Platine des Pi ist die Stromversorgung für das kleine LED-Display außen neben der Eingangstür.

Wenn der Pi zum Debugging ausgebaut werden muss, kriegt man ihn sonst nicht wieder installiert.

{|
|-
|+ Belegung der Pins beim Pi (siehe [https://media.c3d2.de/u/astro/m/rpi-im-schalter/ Bild vom Pi im] [[{{PAGENAME}}|Schalter]] by [[Astro]])
|-
! Reihe
! außen
! innen
|-
| align="right" |1
| frei
| frei
|-
| align="right" |2
| frei
| frei
|-
| align="right" |3
| '''grau''' (keymatic schließen)
| frei
|-
| align="right" |4
| '''hellblau''' (Kabel von der Platine)
| frei
|-
| align="right" |5
| '''gelb'''
| '''dunkelblau''' (Kabel vom Schalter)
|-
| align="right" |6
| '''lila'''
| frei
|-
| align="right" |7
| frei
| frei
|-
| align="right" |8
| '''rot''' (keymatic öffnen)
| frei
|-
| align="right" |9
| frei
| frei
|-
| align="right" |10
| frei
| frei
|-
| align="right" |11
| '''schwarz'''
| frei
|-
| align="right" |12
| frei
| frei
|-
| align="right" |13
| Stromversorgung (N900 - 2. Matemat)
| '''weiß'''
|-
|}

{|
|-
|+ Belegung der Pins bei der Platine<ref>[https://media.c3d2.de/u/astro/m/board-im-schalter/ Bild von der Platine im] [[{{PAGENAME}}|Schalter]] by [[Astro]]</ref>
|-
! Reihe
! außen
! innen
|-
| align="right" |1
| frei
| frei
|-
| align="right" |2
| frei
| frei
|-
| align="right" |3
| '''hellblau'''
| frei
|-
| align="right" |4
| frei
| frei
|-
| align="right" |5
| frei
| frei
|-
| align="right" |6
| frei
| frei
|-
| align="right" |7
| frei
| frei
|-
| align="right" |8
| frei
| frei
|-
| align="right" |9
| frei
| frei
|-
| align="right" |10
| frei
| frei
|-
| align="right" |11
| frei
| frei
|-
| align="right" |12
| frei
| frei
|-
| align="right" |13
| frei
| frei
|-
| align="right" |14
| frei
| frei
|-
| align="right" |15
| frei
| frei
|-
| align="right" |16
| frei
| frei
|-
| align="right" |17
| frei
| frei
|-
| align="right" |18
| frei
| frei
|-
| align="right" |19
| frei
| frei
|-
| align="right" |20
| '''gelb'''
| '''orange'''
|-
| align="right" |21
| '''lila'''
| '''rot'''
|-
| align="right" |22
| frei
| frei
|-
| align="right" |23
| frei
| frei
|-
| align="right" |24
| frei
| frei
|-
| align="right" |25
| frei
| frei
|-
| align="right" |26
| frei
| frei
|-
| align="right" |27
| frei
| frei
|-
| align="right" |28
| frei
| frei
|-
| align="right" |29
| frei
| frei
|-
| align="right" |30
| frei
| frei
|-
| align="right" |31
| frei
| frei
|-
| align="right" |32
| frei
| frei
|-
| align="right" |33
| '''schwarz'''
| '''türkis''' 
|-
| align="right" |34
| '''weiß'''
| '''orange'''
|-
|}


=== Anhalt um den Status von Schalter lokal auszulesen ===

<pre>
p23=$(cat /sys/class/gpio/gpio23/value)
p24=$(cat /sys/class/gpio/gpio24/value)
status=$(expr ${p23} \* 1 + ${p24} \* 2)
echo Status: ${status}
case ${status} in
        0)      echo "Offline";;
        1)      echo "Online";;
        2)      echo "Full";;
        *)      echo "n.d.";;
esac
exit ${status}
</pre>
Nach außen geht es mit dem [https://github.com/astro/spacemsg/tree/master/gpio-sensor gpio-sensor von Astro] auf Port 5555 .
Das Compilat heißt <code>/etc/service/zmq-switch/run</code> .

== Keymatic ==

Um das HQ auch ohne Schlüssel betreten zu können, wurde die Eingangstür mit einem [http://www.elv.de/homematic-funk-tuerschlossantrieb-keymatic-weiss-inkl-funk-handsender.html Funk-Türschlossantrieb] aufgerüstet. Die Keymatic ist über Hackerbus mit dem Raspberry Pi im Schalter verbunden. Damit kann über die GPIO-Pins die Tür auf- und Abgeschlossen werden.

Dabei geht man wie folgt vor:
* per ssh mit dem Pi verbinden
* GPIO-Pins initialisieren (nur nach reboot nötig):
<pre>
echo 18 > /sys/class/gpio/export    // Pin „Aufschließen“
echo 25 > /sys/class/gpio/export    // Pin „Zuschließen“
echo out > /sys/class/gpio/gpio18/direction    // Pins als Ausgang konfigurieren
echo out > /sys/class/gpio/gpio25/direction
</pre>
* Aufschließen:
<pre>
echo "1" > /sys/class/gpio/gpio18/value
// ~20ms warten, entspricht ping -c1 8.8.8.8
echo "0" > /sys/class/gpio/gpio18/value
</pre>
* Zuschließen:
<pre>
echo "1" > /sys/class/gpio/gpio25/value
// ~20ms warten, entspricht ping -c1 8.8.8.8
echo "0" > /sys/class/gpio/gpio25/value
</pre>

== Software ==

=== OpenWrt für den RaspberryPi ===

Wir haben Config & Anleitung um ein OpenWRT für den Pi zu bauen: https://github.com/c3d2/schalter-openwrt

=== senmqd um GPIO im Netzwerk bereitszustellen ===

Der [https://github.com/tuxcodejohn/senmqd Server] für GPIO-Interaktion spricht 0mq.

=== moleflap3 Web Interface ===

[https://github.com/c3d2/moleflap3 Moleflap3] speichert für jeden Zugangsberechtigten nach Namen einen RSA Public Key. Beim Öffnungsversuch gibt der Benutzer den zugehörigen Private Key in seinem Browser ein, der ihn '''nicht''' abschickt, sondern dem Server nur den Versuch mitteilt. Der Server verschlüsselt dann eine Challenge nacheinander für die Pubkeys eines jeden Nutzers. Am Ende beweist der Client dem Server nur dass er die Challenge entschlüsseln kann. Dann schickt der Server den Türöffnungswunsch an den senmqd.

Dieses Schema bietet Anonymisierung des jeweiligen Türöffnenden, jedoch wird das Kopieren von Schlüsseln nicht durch One-Time-Keys verhindert.

Für Keys muss man gerade noch [[Benutzer:Astro|Astro]] ([[Benutzer Diskussion:Astro|Diskussion]]) fragen, und dann kann man das unter http://moleflap.hq.c3d2.de/ benutzen.

==== Zugang beantragen ====

# Private Key erstellen: <code>openssl genrsa -out moleflap.key 2048</code>
# Public Key erstellen: <code>openssl rsa -in moleflap.key -pubout</code>
# Public Key signiert an [[Benutzer:Astro|Astro]] ([[Benutzer Diskussion:Astro|Diskussion]]) mailen

== Siehe auch ==
* [[HQ/Schlüsselersatz]]
* [[HQ/Zugangssystem]]

== Einzelnachweise ==
<references />

{{foo im HQ}}

[[Kategorie:HQ]]
[[Kategorie:Projekt]]
[[Kategorie:Hardware]]
