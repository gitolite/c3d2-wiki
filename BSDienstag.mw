== Idee ==

Es soll Menschen direkt im [[HQ]] und beim [[C3D2]] allgemein und dessen Umfeld geben, die [[BSD]] ([[wikipedia:de:*BSD]]) verwenden.

Das Aufbauen und Etablieren von regelmäßigen Treffen wäre wünschenswert.

Im [[HQ]] laufen auch einige Dienste auf Grundlage von [[FreeBSD]].

=== Regelmäßigkeit ===

* alle 5&nbsp;Wochen?!?
* im [[GCHQ]]
* an einem (BS)Dienstag!

=== Inhalte ===

Angefangen vom trivialen Glotzen von den vergangenen Folgen von [http://bsdnow.tv/ BSD&nbsp;Now] bis hin zum Hacken (bis in die Nacht hinein) von BSD-foo sollte alles möglich sein.

==== mögliche Themenabende ====

BSD bietet viel Potential für [[Themenabend]]e. Exemplarisch sei [https://erdgeist.org/arts/software/ezjail/ ''ezjail'' von ''erdgeist''] benannt. Aber auch allein der andere Ansatz der [[w:de:BSD-Lizenz|BSD-Lizenz]] (gegenüber der [[w:de:GNU General Public License|GNU General Public License]]) wäre "diskussionswürdig".

=== Randerscheinungen ===

==== [[BSD-Crew]] wiederbeleben ====

In Vorzeiten muss (soll) es auch eine aktive [[BSD-Crew]] in Dresden (<code>whois bsd-crew.de</code>) gegeben haben.

== Bezeichnung ==
''BSDienstag'', gesprochen ['biː'ɛsˈdiːnstaːk], also <tt>B</tt> <tt>S</tt> <tt>D</tt> als englischsprachige Abkürzung und <tt>nstag</tt> (was in Verbindung mit dem englischsprachigen <tt>D.</tt> <tt>Dienstag</tt> ergibt) ist eine "Parodie" auf den Spruch <tt>The place to be … S D!</tt> von [http://bsdnow.tv/ BSD&nbsp;Now].

== Projekt ==

=== Initialisierung ===
2014-11-25 fand [[Pentaradio]] zum Thema BSD statt.<ref>[[web:news/pentaradio24-20141125.html]]</ref> Unmittelbar vor der Sendung gab es den Hinweis zu dieser [[#Idee]]. Enthusiastisch wurde spontan ''jeder erste Dienstag'' im Monat ausgerufen.<ref>ab 1:25:30 der [http://ftp.c3d2.de/pentaradio/pentaradio-2014-11-25.ogg Aufzeichnung] vom [[Pentaradio]] 2014-11-25</ref> Ein weiteres konkretes Konzept bestand nicht. Folglich fand 2014-12-02 der [[#1. BSDienstag]] statt.


===einzelne [[BSDienstag]]e===

====1. [[BSDienstag]]====
<ref>[[web:news/event-20141202-bsd.html]]</ref>
; Datum: 2014-12-02&nbsp;20:21
; Ort: [[HQ]]

; Inhalte:
: Es sollen (zum ersten Treffen) keine konkreten Inhalte vorgegen sein (müssen).
* allgemeines Kennenlernen
** erste Resonanz wahrnehmen
** Erwartung (Wünsche) entgegen nehmen
* über gewünschte [[#Inhalte]] (erste Konzeptionierung vom [[BSDienstag]]) sprechen
*: Im Übrigen kann auch die [[Diskussion:{{PAGENAME}}]] auch zum Eintragen von Wünschen genutzt werden.
** entsprechend der Vorstellungen die kommenden [[BSDienstag]]e planen
* Wer möchte, kann [[BSD#BSD Now]] konsumieren oder kommentieren.

; Erlebtes:
* Ein üblicher Tag im [[GCHQ]].
*: Es wurde versäumt eine Mail über den [[Mail-Verteiler]] [[lists:c3d2]] zu versenden.
* [[BSD Now]] lief im [[TV]].
* Spielen mit [[lauterbert]].
* [[Pinguintag]] gefeiert.

==== 2. [[BSDienstag]] ====
<ref>[[web:news/event-20150106-bsd.html]]</ref>
; Datum: 2015-01-06&nbsp;20:15
; Ort: [[HQ]]

==== 3. [[BSDienstag]] ====
<ref>[[web:news/event-20150203-bsd.html]]</ref>
; Datum: 2015-02-03&nbsp;20:15
; Ort: [[HQ]]

==== 4. [[BSDienstag]] ====
<!--<ref>[[web:news/event-20150303-bsd.html]]</ref>-->
; Datum: 2015-03-03&nbsp;20:15
; Ort: [[HQ]]

; Inhalte:
* Was war eigentlich diese [[BSD-Crew]] (Dresden)? :-/
*: Mögen sich alle "Alten" gepiesackt fühlen! (why? - because we can ...) ;-) :-D
** Was ging da so?
** Mag das "reanimieren" werden?
** Gibt es Erwartungshaltung an Neue?
** Was können (wollen) die damaligen Aktiven aktuell machen?

; zu erledigende Dinge:
* im Allgemeinen
** Termin (mit Text zur Ankündigung) für [[C3D2-Web]] erstellen
** Mail (mit Text zur Einladung) an den [[Mail-Verteiler]] [[lists:c3d2]]
** potentiellen (exemplarischen) install&nbsp;foo vorbereiten
* im Speziellen
** gezieltes Weiterleiten der Einladung an Ehemalige der [[BSD-Crew]] mit dem Verweis auf das Zusammenkommen zum Thema ''[[BSD-Crew]] (Dresden)''

==== 5. [[BSDienstag]] ====
<!--<ref>[[web:news/event-20150407-bsd.html]]</ref>-->
; Datum: 2015-04-07&nbsp;20:15
; Ort: [[HQ]]

; Inhalte:
* [[w:bhyve|bhyve]]
*: Wunsch von [[user:vater|vater]] wird von [[user:daniel.plominski|daniel.plominski]] erfüllt.
*: Wollen wir da vielleicht auch gleich einen [[TA]] daraus machen?
** http://bhyve.org/
* [https://2015.eurobsdcon.org/ EuroBSDcon 2015]
*: 2015-10-01 -04 Stockholm
*: Wollen wir da vielleicht hinfahren?
*: [https://2015.eurobsdcon.org/call-for-papers/ Call for Papers] (bis 2015-04-17)
* Gibt es was sehenswertes Neues beim [https://www.youtube.com/user/bsdconferences user ''bsdconferences'' bei youtube]?

==== 6. [[BSDienstag]] ====
<!--<ref>[[web:news/event-20150505-bsd.html]]</ref>-->
; Datum: 2015-05-05&nbsp;20:15
; Ort: [[HQ]]

; Inhalte:
: überwinden!

==== 7. [[BSDienstag]] ====
<!--<ref>[[web:news/event-20150602-bsd.html]]</ref>-->
; Datum: 2015-06-02&nbsp;20:15
; Ort: [[HQ]]

; Inhalte:
: überwinden!

==== 8. [[BSDienstag]] ====
<!--<ref>[[web:news/event-20150707-bsd.html]]</ref>-->
; Datum: 2015-07-07&nbsp;20:15
; Ort: [[HQ]]

; Inhalte:
: überwinden!

==== 9. [[BSDienstag]] ====
<ref>[[web:news/event-20150804-bsd.html]]</ref>
; Datum: 2015-08-04&nbsp;20:15
; Ort: [[HQ]]

; Inhalte:
:; Siehe auch: [[talk:BSDienstag#Sammlung von Themen für den BSDienstag]]

==== 10. [[BSDienstag]] ====
<!--

; zu erledigende Dinge:
* Termin (mit Text zur Ankündigung) für [[C3D2-Web]] erstellen
* Mail (mit Text zur Einladung) an den [[Mail-Verteiler]] [[lists:c3d2]]
* potentiellen (exemplarischen) install&nbsp;foo vorbereiten

-->
<!--<ref>[[web:news/event-20150901-bsd.html]]</ref>-->
; Datum: 2015-09-01&nbsp;20:15
; Ort: [[HQ]]

; Inhalte:
* Den zehnten BSDienstag feiern!
** Was lief bei der ersten Dekade an [[BSDienstag]]en?
** Wollen wir was anders machen?
* Nutzt wer [[FreeBSD]] und ist noch nicht [https://www.freebsd.org/releases/10.2R/relnotes.html auf Version 10.2]?

==== 11. [[BSDienstag]] ====
Thema: adminpit-Projekt

==== 12. [[BSDienstag]] ====
<!--

; zu erledigende Dinge:
* Termin (mit Text zur Ankündigung) für [[C3D2-Web]] erstellen
* Mail (mit Text zur Einladung) an den [[Mail-Verteiler]] [[lists:c3d2]]
* potentiellen (exemplarischen) install&nbsp;foo vorbereiten

-->
<!--<ref>[[web:news/event-20151103-bsd.html]]</ref>-->
; Datum: 2015-11-03&nbsp;20:15
; Ort: [[HQ]]

; Inhalte:
* [[NetBSD]] 7 ist veröffentlicht!<ref>[https://netbsd.org/releases/formal-7/NetBSD-7.0.html NetBSD: Announcing NetBSD 7.0]</ref>
** Testen?
** Kennt wer wen, wer wen kennt, wer [[NetBSD]] betreibt.
* [[OpenBSD]] 5.8 ist veröffentlicht!<ref>[http://www.openbsd.org/58.html OpenBSD: OpenBSD 5.8]</ref>
** Testen?
** Wollen wir eigentlich mal Poster zu [[OpenBSD]] abstauben und das [[HQ]] vollpflastern?
* [[FreeNAS]] 10 ist als alpha zum Testen erschienen.
** vater hat das mal zum Ausprobieren für alle (entsprechend auf einem USB-Stick) installiert.
** Sieht schön aus! :-D
** Klöppeln?
* Frickeln am Projekt [[adminpit]]
* Könnte mal der Text für die Termine (für die [[Website]]) "überarbeitet" werden?
* [[EuroBSDcon]] [https://2016.eurobsdcon.org 2016] findet im September in [[wikipedia:de:Belgrad]] statt.
** Barrieren brechen! Grenzen überwinden!

==== 13. [[BSDienstag]] ====
<!--

; zu erledigende Dinge:
* Termin (mit Text zur Ankündigung) für [[C3D2-Web]] erstellen
* Mail (mit Text zur Einladung) an den [[Mail-Verteiler]] [[lists:c3d2]]
* potentiellen (exemplarischen) install&nbsp;foo vorbereiten

-->
<!--<ref>[[web:news/event-20151103-bsd.html]]</ref>-->
; Datum: 2015-12-01&nbsp;20:15
; Ort: [[HQ]]

; Inhalte:
* [[32C3]]
** assembly zu BSD anstiften?
*** BSD Foundation Germany ausmalen?
*** Menschen zum [[BSDienstag]] als Themenabend einladen?

==== 14. [[BSDienstag]] ====

; zu erledigende Dinge:
* Termin (mit Text zur Ankündigung) für [[C3D2-Web]] erstellen
** [[user:vater]] schreibt mal einen anderen Text.
**:
<source lang=html5>
 <h3>
Was ist der BSDienstag?
 </h3>
 <p>
Beim 
  <a href="https://c3d2.de">
C3D2
  </a>
 gibt es Menschen und Maschinen, die BSD nutzen.
 </p>
 <p>
Jeden ersten Dienstag im Monat (gemäß gregorianischen Kalenders) treffen sich einige dieser Menschen. Das Treffen nennt sich BSDienstag und findet im HQ statt. Gern dürfen auch Maschinen mitgebracht werden, wo BSD verwendet wird.
 </p>
 <p>
Wir erlauben es uns nicht uns als "BSD-Crew" zu bezeichnen, aber würden es gern.
  <br />
Die Anzahl von BSD-Kindern ist überschaubar. Alle sind lieb, lernen gern, spielen viel und sind pflegeleicht. Leider bilden wir höchstens den "Mainstream" zu BSD ab. Fast alles bezieht sich auf FreeBSD. Gern kann das geändert werden. Wir finden Vielfalt gut!
 </p>
 <p>
Häufig versuchen wir in der Vorbereitung ein Thema zu benennen. Aber grundsätzlich kann jederzeit alles gemacht werden. Einige ausgewählte Beispiele:
  <ul>
   <li>
Installation von BSD (Das Erstellen von einem Startmedium für USB für das jeweils gewünschte BSD ist ja eine Sache von wenigen Minuten.)
   </li>
   <li>
Glotzen von 
    <a href="https://bsdnow.tv">
BSDNow
    </a>
   </li>
   <li>
Frickeln an 
    <a href="https://wiki.c3d2.de/yocage">
yocage
    </a>
 aka 
    <a href="https://redmine.c3d2.de/projects/adminpit/">
admninpit
    </a>
   </li>
   <li>
Aufgaben von den Projekten erledigen (von einfachen Übersetzen des Handbuches von FreeBSD, über Bugs fixen von FreeNAS, bis hin zum Mitentwickeln bei PC-BSD)
   </li>
  </ul>
 </p>
 <p>
  <a href="https://wiki.c3d2.de/BSDienstag">
Detaillierte Informationen zum BSDienstag sind im Wiki zu finden.
  </a>
 </p>
 <h3>
Was passiert zu diesem BSDienstag?
 </h3>
 <p>
Was ist eigentlich 
  <a href="https://smartos.org">
SmartOS
  </a>
 ?
  <br />
Hat wer schon einmal 
  <a href="https://project-fifo.net/">
FiFo
  </a>
 genutzt?
  <br />
Wollen wir uns das anschauen?
  <br />
Das ist ja mit aus der Feder von 
  <a href="https://en.wikipedia.org/wiki/Bryan_Cantrill">
Bryan Cantrill
  </a>
, der ja mit seiner Vortragsweise spacegirlz zum fanboy werden lässt.
 </p>
 <h3>
Was geht denn so zu BSD beim C3D2?
 </h3>
 <p>
Eigentlich passiert sonst nicht besonders viel zu BSD beim C3D2. Da könnte viel mehr gehen!
 </p>
 <p>
Wenn du Lust hast da was zu machen, dann
  <a href="mailto:c3d2@lists.c3d2.de">
schreib doch direkt an den öffentlichen Mail-Verteiler vom C3D2
  </a>
. Einfach zum BSDienstag vorbeikommen, ist selbstverständlich auch erwünscht. Beides machen ist perfekt.
  <br />
Gewisse Ideen beziehungsweise Projekte haben wir auch, wo du dich gern mit einbringen kannst.
 </p>
 <p>
Chaos ohne BSD kann ja auch kein Mensch wollen.
 </p>
 <p>
Zum Einstieg kann 
  <a href="https://www.c3d2.de/news/pentaradio24-20141125.html">
die Sendung Pentaradio zum Thema 
   <i>
BSD
   </i>
 (2014-11-25)
  </a>
 dienen. Auch gibt es 
  <a href="https://wiki.c3d2.de/BSD">
den allgemeinen Artikel 
   <i>
BSD
   </i> im Wiki
  </a>
. Für das selbstständige Testen von Unerfahrenen kann 
  <a href="https://web.pcbsd.org/?lang=de">
die Distribution 
   <i>
PC-BSD
   </i>
  </a>
 empholen werden.
 <br />
Bei Fragen kann ja auch einfach zum BSDienstag vorbeigeschaut werden.
 </p>
 <p>
Aber auch beim sonstigen Betrieb im HQ kommt BSD zum Einsatz.
  <br />
Also das Finden von BSD-Gleichgesinnten, das Hacken mit und an BSD ist häufiger bis tief in die Nacht möglich.
 </p>
</source>
** [[user:PT]] stellt den Text beim [[C3D2-Web]] online
<ref>[[web:news/event-20160105-bsd.html]]</ref>
; Datum: 2016-01-05&nbsp;21:42
; Ort: [[HQ]]

; Inhalte:
* [[33C3]]
** assembly zu [[BSD]] anstiften?
*** BSD Foundation Germany ausmalen?
* [[SmartOS]]
** i.V.m. [[FiFo]]

==== 15. [[BSDienstag]] ====

; zu erledigende Dinge:
* Termin (mit Text zur Ankündigung) für [[C3D2-Web]] erstellen
** [[user:vater]] schreibt mal einen anderen Text.
**:
<source lang=html5>
 <h3>
Was ist der BSDienstag?
 </h3>
 <p>
Beim 
  <a href="https://c3d2.de">
C3D2
  </a>
 gibt es Menschen und Maschinen, die BSD nutzen.
 </p>
 <p>
Jeden ersten Dienstag im Monat (gemäß gregorianischen Kalenders) treffen sich einige dieser Menschen. Das Treffen nennt sich BSDienstag und findet im HQ statt. Gern dürfen auch Maschinen mitgebracht werden, wo BSD verwendet wird.
 </p>
 <p>
Wir erlauben es uns nicht uns als "BSD-Crew" zu bezeichnen, aber würden es gern.
  <br />
Die Anzahl von BSD-Kindern ist überschaubar. Alle sind lieb, lernen gern, spielen viel und sind pflegeleicht. Leider bilden wir höchstens den "Mainstream" zu BSD ab. Fast alles bezieht sich auf FreeBSD. Gern kann das geändert werden. Wir finden Vielfalt gut!
 </p>
 <p>
Häufig versuchen wir in der Vorbereitung ein Thema zu benennen. Aber grundsätzlich kann jederzeit alles gemacht werden. Einige ausgewählte Beispiele:
  <ul>
   <li>
Installation von BSD (Das Erstellen von einem Startmedium für USB für das jeweils gewünschte BSD ist ja eine Sache von wenigen Minuten.)
   </li>
   <li>
Glotzen von 
    <a href="https://bsdnow.tv">
BSDNow
    </a>
   </li>
   <li>
Frickeln an 
    <a href="https://wiki.c3d2.de/yocage">
yocage
    </a>
 aka 
    <a href="https://redmine.c3d2.de/projects/adminpit/">
admninpit
    </a>
   </li>
   <li>
Aufgaben von den Projekten erledigen (von einfachen Übersetzen des Handbuches von FreeBSD, über Bugs fixen von FreeNAS, bis hin zum Mitentwickeln bei PC-BSD)
   </li>
  </ul>
 </p>
 <p>
  <a href="https://wiki.c3d2.de/BSDienstag">
Detaillierte Informationen zum BSDienstag sind im Wiki zu finden.
  </a>
 </p>
 <h3>
Was passiert zu diesem BSDienstag?
 </h3>
 <p>
Was passiert eigentlich auf dem Congress
  <a href="https://events.ccc.de/category/congress/">
Congress
  </a>
 zu BSD? Wohl nicht mehr viel!
  <br />
Wollen wir das ändern? Ein 
  <a href="https://wiki.c3d2.de/33C3/Assembly:BSD">
33C3 Assembly zu BSD auf dem 33C3
  </a>
 machen?
 </p>
 <p>
  <a href="https://www.freebsd.org/releases/11.0R/schedule.html">  
In einem halben Jahr (Ende Juli) soll FreeBSD&nbsp;11 erscheinen!
  </a>
  <br />
Wollen wir uns eine Art 
  <i>
release&nbsp;party
  </i>
, ähnlich 
  <a href="https://www.c3d2.de/news/event-20150425-debian-jessie-release.html">
wie es sie für Debian&nbsp;8 gab,
  </a>
 machen?
  <br />
Ähnlich könnten wir das auch schon einmal zum 1.&nbsp;Mai&nbsp;2016 für 
  <a href="http://www.openbsd.org/59.html">
OpenBSD&nbsp;5.9
  </a>
einladen.
 </p>
 <h3>
Was geht denn so zu BSD beim C3D2?
 </h3>
 <p>
Eigentlich passiert sonst nicht besonders viel zu BSD beim C3D2. Da könnte viel mehr gehen!
 </p>
 <p>
Wenn du Lust hast da was zu machen, dann
  <a href="mailto:c3d2@lists.c3d2.de">
schreib doch direkt an den öffentlichen Mail-Verteiler vom C3D2
  </a>
. Einfach zum BSDienstag vorbeikommen, ist selbstverständlich auch erwünscht. Beides machen ist perfekt.
  <br />
Gewisse Ideen beziehungsweise Projekte haben wir auch, wo du dich gern mit einbringen kannst.
 </p>
 <p>
Chaos ohne BSD kann ja auch kein Mensch wollen.
 </p>
 <p>
Zum Einstieg kann 
  <a href="https://www.c3d2.de/news/pentaradio24-20141125.html">
die Sendung Pentaradio zum Thema 
   <i>
BSD
   </i>
 (2014-11-25)
  </a>
 dienen. Auch gibt es 
  <a href="https://wiki.c3d2.de/BSD">
den allgemeinen Artikel 
   <i>
BSD
   </i> im Wiki
  </a>
. Für das selbstständige Testen von Unerfahrenen kann 
  <a href="https://web.pcbsd.org/?lang=de">
die Distribution 
   <i>
PC-BSD
   </i>
  </a>
 empholen werden.
 <br />
Bei Fragen kann ja auch einfach zum BSDienstag vorbeigeschaut werden.
 </p>
 <p>
Aber auch beim sonstigen Betrieb im HQ kommt BSD zum Einsatz.
  <br />
Also das Finden von BSD-Gleichgesinnten, das Hacken mit und an BSD ist häufiger bis tief in die Nacht möglich.
 </p>
</source>
** [[user:PT]] stellt den Text beim [[C3D2-Web]] online
<ref>[[web:news/event-20160105-bsd.html]]</ref>
; Datum: 2016-01-05&nbsp;21:42
; Ort: [[HQ]]


; Inhalte
* Menschen zum [[BSDienstag]] als Themenabend einladen?
* [[33C3/Assembly:BSD]]

=== Aktive ===

* [[user:vater|vater]]
* [[user:daniel.plominski|daniel.plominski]]
* [[user:PwnyTail|PT]]
* [[user:TonyBoston|tboston]]

==== Interessierte ====

* ~~

== Siehe auch ==

* [[:Kategorie:BSD]]

== Einzelnachweise ==
<references />

[[Kategorie:Projekt]]
[[Kategorie:BSD]]
