[[Kategorie:Wissen]]
== SSH-Verbindungen recyceln==

In aktuellen SSH-Versionen (ab 4.0) gibt es die Möglichkeit, SSH-Verbindungen wiederzubenutzen: Hat man bereits eine Shell auf einem entfernten Rechner offen und will dann z.B. '''scp''' benutzen, um etwas dorthin zu kopieren, wird die gleiche Verbindung benutzt. Dadurch muss man sich nicht noch einmal an dem anderen Rechner authentifizieren, was einiges an Zeit spart. Damit alle zukünftigen SSH-Sitzungen davon profitieren, ein Verzeichnis '''~/.ssh/sockets''' anlegen und dann Folgendes in die '''~/.ssh/config''' schreiben:

 Host *
   ControlPath ~/.ssh/sockets/%r@%h:%p
   ControlMaster auto

Leider löscht ssh Socket-Leichen nicht automatisch, sollte einmal ein Prozess sterben, und weigert sich dann, eine weitere Verbindung zum entsprechenden Host aufzubauen. Eine entsprechende Fehlermeldung sieht z.B. so aus:

 Control socket connect(/home/frank/.ssh/sockets/toidinamai@172.22.99.2:22): Connection refused
 ControlSocket /home/frank/.ssh/sockets/toidinamai@172.22.99.2:22 already exists

In diesem Fall einfach die störende Socket-Datei löschen. Sollte es aus irgend einem Grund nötig sein, eine separate Sitzung aufzubauen, kann man das mit

 ssh -o "ControlPath none" user@host

tun.

'''''Achtung:''''' Die erste aufgebaute Sitzung fungiert als Master für alle weiteren. Wird sie geschlossen, sterben auch alle anderen Verbindungen zum gleichen Host.

== SSH über Tor Hidden Services / torify SSH ==

* Tor starten (Annahme hier: läuft auf 9050/tcp)
* Folgenden Abschnitt in die '''~/.ssh/config''' packen:
 Host *.onion 
   ProxyCommand socat STDIO SOCKS4A:localhost:%h:%p,socksport=9050
* Um zu aromaster zu ssh'en:
 ssh hacker@6kgmplpcyjpesalg.onion

''Tor ist leider recht lahm und auch nicht jeder Verbindungsversuch ist erfolgreich.''

==SSH ins ChaosVPN==

Dazu wird ein öffentlich erreichbarer "Gateway" benötigt. In diesem Beispiel soll das example.net sein, dort muss [[netcat]] als '''nc''' installiert sein.

Folgende Zeilen gehören in die '''~/.ssh/config''':
 Host *.diac24
   ProxyCommand ssh user@example.net "nc $(echo %h|sed -e 's/\.diac24$//' ) %p"

Um nun auf aromaster zu verbinden:
 ssh user@172.22.99.2.diac24

== Authentifizierungsmethode ==

Normalerweise möchte man gerne mit Keyfile aithentifizieren. Der Key wird entweder mit der Option <code>-i [keyfile]</code> angegeben oder automatisch durch ausprobieren ermittelt.

Problem 1: Wenn Keyfiles ausprobiert werden, wird ab einer durch den Server festgelegten Anzahl an Versuchen abgebrochen. Lösung 1: die Option <code>-i</code>

Problem 2: Wenn man keinen Key auf dem Server hat wird dann nicht nach Passwort gefragt. Lösung 2: Option <code>-o PreferredAuthentications="password"</code>
