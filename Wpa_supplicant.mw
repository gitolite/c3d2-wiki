{{SEITENTITEL:wpa_supplicant}}
[[wpa_supplicant]] ist eine Variante, wie Verbindungen mit dem WLAN verwaltet werden können.

== Vergleich mit anderen Diensten ==

=== allgemeine Vor- und Nachteile ===

; allgemeine Vorteile:
* via [[w:de:Kommandozeile|Kommandozeile]] zu benutzen
* [[#Konfiguration | Konfigurationsdatei <code>wpa_supplicant.conf</code>]] ist einfache [[w:de:Textdatei|Textdatei]]
* Funktionalität zur möglichen Priorisierung von Netzwerken
* brauchbare Dokumentation
* Anreiz Netzwerkkonfiguration auch manuell vorzunehmen
* grafisches Frontend mit [[#wpa_supplicant-gui]] verfügbar

; allgemeine Nachteile:
* bei Verbindungsproblemen ist ggf. der Dienst <tt>wpa_supplicant</tt> im Weg

=== Vergleich mit NetworkManager ===

Im Vergleich zum NetworkManager ([http://manpages.debian.org/cgi-bin/man.cgi?query=networkmanager debianman:networkmanager])<ref>https://wiki.debian.org/NetworkManager</ref>, beispielsweise unter [[w:de:Gnome|Gnome]]<ref>https://wiki.gnome.org/Projects/NetworkManager</ref> ([https://packages.debian.org/search?keywords=network-manager-gnome package <code>network-manager-gnome</code>]), ist [[wpa_supplicant]] weniger fehleranfällig und leichter direkt aus dem Terminal konfigurierbar.

== Installation ==

=== Kompatibilität ===

Vorab sollte anhand der [http://hostap.epitest.fi/wpa_supplicant/ Liste beim Projekt] geprüft werden, ob eure Hardware auch kompatibel ist.

=== Debian ===

;Pakete:
* wpasupplicant (Service und Tools)
* wpagui (grafisches Frontend)

; Paket installieren:
: <source lang=bash>sudo apt-get update</source>
: <source lang=bash>sudo apt-get install wpasupplicant</source>
; Rechte auf Interfaces beschränken:
: <source lang=bash>sudo chmod 0600 /etc/network/interfaces</source>
; wpa_supplicant.conf erstellen:
: siehe [[#Konfiguration]]
; [[Network-Interface|Interface]]s prüfen:
:: <source lang=bash>cat /etc/network/interfaces</source>
: oder setzen:
:: <source lang=bash>sudo vi /etc/network/interfaces</source>
<pre>
# The loopback network interface
auto lo
iface lo inet loopback

auto wlan0
iface wlan0 inet dhcp
  wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
# EOF
</pre>
: alternativ s.a. [[Network-bonding-failover]] um ''ohne Verbindungsabbruch zwischen LAN und WLAN wechseln'' zu können.
; Netzwerkdienst neustarten:
: <source lang=bash>sudo /etc/init.d/networking restart</source>

== Konfiguration ==

Die Konfigurationsdatei (Standard: <code>/etc/wpa_supplicant/wpa_supplicant.conf</code>) muss nach jeder Änderung erst neu eingelesen werden um wirksam zu werden. Mit <code>wpa_cli</code> kann <code>wpa_supplicant</code> direkt gesteuert werden.

; erstmal ansehen
: <source lang=bash>cat /etc/wpa_supplicant/wpa_supplicant.conf</source>

; Die Datei kann neben den Netzwerken im Kopf ein paar allg. Angaben enthalten:

<pre>
# general config

ctrl_interface=/var/run/wpa_supplicant

ctrl_interface_group=0

# activily scanning for ap
ap_scan=1

fast_reauth=1

# compatibility to older ap
eapol_version=1

# allow updating config by wpa_cli
# update_config=1
</pre>

==== Priorisierung ====

Für jedes Netzwerk kann über den Wert <code>priority=x</code> ein Wert <code>x</code> (Ganze Zahlen -ge 0) definiert werden. Die Verbindung wird zum Netzwerk mit der höchsten Priorität zuerst aufgebaut, bei gleicher Priorität wird die Reihenfolge durch die Eintragung in der Konfiguration bestimmt.

==== Keine / Doppelte SSID ====

Um eine unerwünschte Verbindung zu APs mit gleicher SSID zu verhindern kann die BSSID definiert werden.
Wenn der AP keine SSID sendet, muss für das Netzwerk mit <code>scan_ssid=1</code> aktiv nach der SSID gescannt werden.

=== Offenes WLAN konfigurieren ===

die Datei kann so zum [[WLAN#WLAN_C3D2|freien WLAN im HQ]] führen:

<source lang=bash>vi /etc/wpa_supplicant/wpa_supplicant.conf</source>
Und anhängen von..
<pre>
# general unsec network ####
# This one is for connecting to any unsecured network your machine comes into contect with, just comment out if you don't like this: 
#network={
#        key_mgmt=NONE
#        priority=0
#        id_str="unsecure open wifi"
#}

# ## ### ### C3D2 ### ### ## #
network={
        ssid="C3D2"
        key_mgmt=NONE
        priority=2
        id_str="C3D2"
}
# ## ### ### C3D2 ### ### ## #
#EOF </pre>

=== Passwortgeschütztes WLAN konfigurieren ===

==== mit <code>[[man:wpa_passphrase|wpa_passphrase]]</code> ====

: am Bsp. [[C3D2-fallback]] als Einzeiler

''so kann die Konfigurationsdatei für ein Netzwerk erweitert werden,''
Der Prompt erwartet die Passphrase

<source lang=bash>wpa_passphrase C3D2-fallback >> /etc/wpa_supplicant/wpa_supplicant.conf</source>

''entsprechend ohne Prompt'' - '''das Passwort wird sowohl in der Shell History als auch in der Konfigurationsdatei im Klartext abgelegt'''

<source lang=bash>echo "zweimalDasUebliche" | wpa_passphrase C3D2-fallback >> /etc/wpa_supplicant/wpa_supplicant.conf</source>

==== mit <code>[[man:wpa_cli|wpa_cli]]</code> ====

<code>wpa_cli</code> ist eher für die ambitionierte Anwenderin und zum Testen zu empfehlen. Konfigurationsdatei zuvor sichern!

''Vorgehensweise:''

* konfigurierte Netzwerke listen
* Scannen anschalten
* verfügbare Netzwerke anzeigen
* neue Konfiguration anlegen (Ausgegebene id wird weiterverwendet, hier 0)
* SSID für Netzwerk mit id setzen
* PSK für Netzwerk mit id setzen
* Priorität festlegen (Ganzzahl >= 0; höhere wird eher verbunden)
* Netzwerk auswählen
* Netzwerk aktivieren
* Konfiguration speichern (benötigt Eintrag <code>update_config=1</code> in Konfigurationsdatei, nicht empfohlen)

''unter <code>wpa_cli</code>''

<pre>
list_networks
scan
scan_results
add_network
set_network 0 ssid "WLAN-Network"
set_network 0 psk "SupahSecretPassphrase"
set_network 0 priority 0
select_network 0
eneable_network 0
save_config
</pre>

== Deaktivieren ==

Um das [[Netzwerk manuell konfigurieren]] zu können, muss WPA Supplicant zunächst deaktiviert werden.

<!-- Bitte hier sinnvoll ergänzen. -->
<source lang=bash>
???
</source>

== wpa_supplicant-gui ==

Für die grafische Bedienung gibt es auch eine Erweitung.

<!-- Doku erwünscht -->

== Quellen ==

* wpa_supplicant im [https://wiki.debian.org/WiFi/HowToUse#wpa_supplicant Debian-Wiki]
* [http://sirlagz.net/2012/08/27/how-to-use-wpa_cli-to-connect-to-a-wireless-network/ wpa_cli erläutert]
* [http://www.lsi.upc.edu/lclsi/Manuales/wireless/files/wpa_supplicant.conf Beispiel-Konfigurationsdatei]

== Einzelnachweise ==
<references />

[[Kategorie:Netzwerk]]
